//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAO
{
    using System;
    using System.Collections.Generic;
    
    public partial class GiaoVien
    {
        public GiaoVien()
        {
            this.ChiTietPhanCongs = new HashSet<ChiTietPhanCong>();
        }
    
        public int MaGV { get; set; }
        public string TenGV { get; set; }
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public Nullable<int> PhanQuyen { get; set; }
    
        public virtual ICollection<ChiTietPhanCong> ChiTietPhanCongs { get; set; }
    }
}
