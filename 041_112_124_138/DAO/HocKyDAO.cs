﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class HocKyDAO
    {
        public List<HocKy> get_ALL_HK()
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                return ctx.HocKies.Include("MonHocs").ToList();

            }

        }

        public void addHK(HocKy hk)
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                ctx.HocKies.Add(hk);
                ctx.SaveChanges();
            }
        }
        public void editHK(HocKy hk)
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                HocKy hknew = ctx.HocKies.Where(m => m.MaHK == hk.MaHK).FirstOrDefault();
                hknew.MaHK = hk.MaHK;
                hknew.TenHK = hk.TenHK;
                hknew.SL_MonHocMax = hk.SL_MonHocMax;
                ctx.SaveChanges();
            }
        }
        public void deleteHk(int mahk)
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                HocKy hknew = ctx.HocKies.Where(m => m.MaHK == mahk).FirstOrDefault();
                ctx.HocKies.Remove(hknew);
                ctx.SaveChanges();
            }
        }
        public HocKy getHocKy_byMaHk(int maHK)
        {

            using (QLHSEntities ctx = new QLHSEntities())
            {
                HocKy hknew = ctx.HocKies.Where(hk => hk.MaHK == maHK).FirstOrDefault();
                return hknew;
            }
        }
    }
}
