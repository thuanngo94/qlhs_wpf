﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class MonHocDAO
    {
        public List<MonHoc> get_ALL_MH()
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                return ctx.MonHocs.Include("Diems").Include("HocKy").ToList();
            }
        }

        public ICollection<MonHoc> get_ALL_MH_TheoMaHK(int Mahk)
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                return ctx.MonHocs.Include("Diems").Include("HocKy")
                    .Where(mh => mh.MaHK == Mahk).ToList();
            }
        }

        public void addMH(MonHoc mh)
        {

            using (QLHSEntities ctx = new QLHSEntities())
            {
                ctx.MonHocs.Add(mh);
                HocKy hk = ctx.HocKies.Where(h => h.MaHK == mh.MaHK).FirstOrDefault();
                hk.SL_MonHoc += 1;
                ctx.SaveChanges();
            }
        }

        public void editMH(MonHoc mh)
        {

            using (QLHSEntities ctx = new QLHSEntities())
            {
                MonHoc mhnew = ctx.MonHocs.Where(m => m.MaMH == mh.MaMH).FirstOrDefault();

                // cap nhat lai soluong hoc ky
                HocKy hk = ctx.HocKies.Where(h => h.MaHK == mhnew.MaHK).FirstOrDefault();
                hk.SL_MonHoc -= 1;

                mhnew.MaMH = mh.MaMH;
                mhnew.TenMH = mh.TenMH;
                mhnew.MaHK = mh.MaHK;
                mhnew.DiemChuan = mh.DiemChuan;
                HocKy hk1 = ctx.HocKies.Where(h => h.MaHK == mh.MaHK).FirstOrDefault();
                hk1.SL_MonHoc += 1;

                ctx.SaveChanges();
            }
        }

        public void deleteMH(int mh)
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                MonHoc mhnew = ctx.MonHocs.Where(m => m.MaMH == mh).FirstOrDefault();

                HocKy hk = ctx.HocKies.Where(h => h.MaHK == mhnew.MaHK).FirstOrDefault();
                hk.SL_MonHoc -= 1;

                ctx.MonHocs.Remove(mhnew);
                ctx.SaveChanges();
            }
        }
        public String kiemtra_xoa(int maMH)
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                Diem diem = ctx.Diems.Where(d => d.MaMH == maMH).FirstOrDefault();
                if (diem != null)
                {
                    return "Môn học này đang chứa thông tin điểm nên ko thể xóa";
                }
                ChiTietPhanCong PC = ctx.ChiTietPhanCongs.Where(pc => pc.MaMH == maMH).FirstOrDefault();
                if (PC != null)
                {
                    return "Môn học nay đang được phân công cho giáo viên nên không thể xóa";
                }
                return "true";

            }
        }

        public bool kiemtra_SoluongMH(int mahk)
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                HocKy hocky = ctx.HocKies.Where(hk => hk.MaHK == mahk).FirstOrDefault();
                if(hocky.SL_MonHoc < hocky.SL_MonHocMax)
                {
                    return true;
                }
                return false;
            }
        }


    }
}
