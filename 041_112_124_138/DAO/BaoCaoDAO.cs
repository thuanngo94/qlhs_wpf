﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class BaoCaoDAO
    {
        public IEnumerable<Object> getall_TKMon(int malop, int mahk)
        {
            
            IEnumerable<Object> lst = null;
            using (QLHSEntities ctx = new QLHSEntities())
            {

                lst = from hs in ctx.HocSinhs
                      from mh in ctx.MonHocs
                      from d in ctx.Diems
                      from hk in ctx.HocKies
                      from l in ctx.Lops
                      where hs.MaHS == d.MaHS
                      where d.MaMH == mh.MaMH
                      where mh.MaHK == hk.MaHK
                      where l.MaLop == hs.MaLop
                      where d.DiemCuoiHk >= 5
                      group new { hs, mh, d, hk,l } by new { l.MaLop, mh.TenMH,hk.TenHK, l.Siso } into g

                      select new
                      {
                          MaLop = g.Key.MaLop,
                          TenMH = g.Key.TenMH,
                          TenHK = g.Key.TenHK,
                          Siso = g.Key.Siso,
                          SLDat = g.Count(),
                          Tyle = g.Count()*100/g.Key.Siso
                      };

            }
            return lst;
            
        }
    }
}
