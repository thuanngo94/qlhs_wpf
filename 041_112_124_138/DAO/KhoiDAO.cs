﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
     public class KhoiDAO
    {
         public List<Khoi> getAll_Khoi()
         {
             using (QLHSEntities ctx = new QLHSEntities())
             {
                 return ctx.Khois.Include("Lops").ToList();
             }
         }
         public int soluonglop (int makhoi)
         {
             using (QLHSEntities ctx = new QLHSEntities())
             {
                 Khoi khoi = ctx.Khois.Where(p => p.MaKhoi == makhoi).FirstOrDefault();
                 if (khoi.SL_Lop < khoi.SL_LopToiDa)
                     return 1;
                 else
                     return 0;
             }
         }
         public int xoakhoi(int makhoi)
         {
             using (QLHSEntities ctx = new QLHSEntities())
             {
                 var lp = ctx.Lops.Where(p => p.MaKhoi == makhoi).ToList();
                 if (lp.Count() > 0)
                 {
                     return 0;
                 }
                 else
                 {
                     Khoi k1 = ctx.Khois.Where(p => p.MaKhoi == makhoi).FirstOrDefault();
                     if (k1 != null)
                     {
                         ctx.Khois.Remove(k1);
                     }
                     ctx.SaveChanges();
                     return 1;
                 }
             }
         }
         public int themkhoi(Khoi a)
         {
             Khoi p = new Khoi
             {
                 TenKhoi = a.TenKhoi,
                 TuoiToiThieu = a.TuoiToiThieu,
                 TuoiToiDa = a.TuoiToiDa,
                 SL_Lop = 0,
                 SL_LopToiDa = a.SL_LopToiDa
             };
             using (QLHSEntities ctx = new QLHSEntities())
             {
                 ctx.Khois.Add(p);
                 ctx.SaveChanges();
                 return 1;
             }
         }
         public int suakhoi(Khoi k1)
         {
             using (QLHSEntities ctx = new QLHSEntities())
             {
                 Khoi k2 = ctx.Khois.Where(p => p.MaKhoi == k1.MaKhoi).FirstOrDefault();
                 if (k2.SL_Lop > k1.SL_LopToiDa)
                     return 0;
                 else
                 {
                     k2.TenKhoi = k1.TenKhoi;
                     k2.TuoiToiThieu = k1.TuoiToiThieu;
                     k2.TuoiToiDa = k1.TuoiToiDa;
                     k2.SL_LopToiDa = k1.SL_LopToiDa;
                     ctx.SaveChanges();
                     return 1;
                 }
                 
             }
         }
    }
}
