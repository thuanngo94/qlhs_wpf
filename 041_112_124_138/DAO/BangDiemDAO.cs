﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class BangDiemDAO
    {
        public List<Diem> getAll_Diem()
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                return ctx.Diems.Include("HocSinh").ToList();
            }
        }
        public List<Diem> getMaLop_Diem(int malop, int mahk, int mamh)
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                List<Diem> list = ctx.Diems.ToList();
                //string query = "Select d.* from Diem d, HocSinh hs, MonHoc mh where d.MaHS = hs.MaHS and d.MaMH = mh.MaMH ";
                if (malop != 0)
                {
                    list = list.Where(d => d.HocSinh.MaLop == malop).ToList();
                    //query += string.Format("and hs.MaLop ={0}", malop);
                }
                if (mahk != 0 && list.Count != 0)
                {

                    list = list.Where(d => d.MonHoc.MaHK == mahk).ToList();
                    //query += string.Format("and mh.MaHK ={0}", mahk);
                }
                if (mamh != 0 && list.Count != 0)
                {
                    list = list.Where(d => d.MaMH == mamh).ToList();
                    //query += string.Format("and d.MaMH ={0}", mamh);
                }

                return list;
            }
        }
        public void addDiem(Diem d)
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                ctx.Diems.Add(d);
                ctx.SaveChanges();
            }
        }
        public void EditDiem(Diem d)
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                Diem d2 = ctx.Diems.Where(d1 => d1.MaHS == d.MaHS && d.MaMH == d.MaMH).FirstOrDefault();

                d2.Diem1tiet = d.Diem1tiet;
                d2.Diẹm15 = d.Diẹm15;
                d2.DiemCuoiHk = d.DiemCuoiHk;
                ctx.SaveChanges();
            }
        }

        public void DeleteDiem(Diem d)
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                Diem d2 = ctx.Diems.Where(d1 => d1.MaHS == d.MaHS && d1.MaMH == d.MaMH).FirstOrDefault();

                ctx.Diems.Remove(d2);
                ctx.SaveChanges();
            }
        }
        public  List<MonHoc> LayDS_Monhoc_ChuaNhapDiem_TheoHocKy_cuaHocSinh(int maHS, int maHK)
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                var all_lst = (from mh in ctx.MonHocs
                                           from hk in ctx.HocKies
                                           from d in ctx.Diems
                                           where mh.MaHK == hk.MaHK
                                           where d.MaMH == mh.MaMH
                                           where d.MaHS == maHS
                                           where hk.MaHK == maHK
                                           select mh.MaMH).ToList();


                var lst1 = from mh in ctx.MonHocs
                                          where !all_lst.Contains(mh.MaMH)
                                          where mh.MaHK == maHK
                                          select mh;
                List<MonHoc> lst_MH_chuanhapdiem = lst1.Cast<MonHoc>().ToList();
                return lst_MH_chuanhapdiem;
            }
        }
    }
}
