﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class HocSinhDAO
    {
        static int SoluongMH_HK1 ;
        static int SoluongMH_HK2 ;
       // static Expression<Func<float, string>> convertTodigit = a => String.Format("{0:0.00}", a);
        public IEnumerable<Object> getall_hs_DTB(string TenHocSinh, int malop, int kiemtra)
        {
            Expression<Func<float, string>> convertTodigit = a => String.Format("{0:0.00}", a);
            
            SoluongMH_HK1 = SoluongMonhoc_cuaHK(1);
            SoluongMH_HK2 = SoluongMonhoc_cuaHK(2);
            IEnumerable<Object> lst = null;
            using (QLHSEntities ctx = new QLHSEntities())
            {

                if (kiemtra == 1)
                {
                    lst = from hs in ctx.HocSinhs
                          from mh in ctx.MonHocs
                          from d in ctx.Diems
                          from hk in ctx.HocKies
                          from l in ctx.Lops
                          where hs.MaHS == d.MaHS
                          where d.MaMH == mh.MaMH
                          where mh.MaHK == hk.MaHK
                          where l.MaLop == hs.MaLop
                          group new { hs, mh, d, hk } by new { hs.MaHS, hs.TenHS, l.TenLop } into g

                          select new
                          {
                              TenHS = g.Key.TenHS,
                              TenLop = g.Key.TenLop,
                              TBHK1 = ((g.Where(r => r.hk.MaHK == 1).Sum(r => r.d.DiemCuoiHk)) / SoluongMH_HK1),
                              TBHK2 = (g.Where(r => r.hk.MaHK == 2).Sum(r => r.d.DiemCuoiHk)) / SoluongMH_HK2
                          };

                }

                //grdHocSinh.ItemsSource = lst.ToList();

                else if (kiemtra == 2)
                {

                    lst = from hs in ctx.HocSinhs
                                                 .Where(h => h.TenHS.Contains(TenHocSinh) && h.MaLop == malop)
                          from mh in ctx.MonHocs
                          from d in ctx.Diems
                          from hk in ctx.HocKies
                          from l in ctx.Lops
                          where hs.MaHS == d.MaHS
                          where d.MaMH == mh.MaMH
                          where mh.MaHK == hk.MaHK
                          where l.MaLop == hs.MaLop

                          group new { hs, mh, d, hk } by new { hs.MaHS, hs.TenHS, l.TenLop } into g

                          select new
                          {
                              TenHS = g.Key.TenHS,
                              TenLop = g.Key.TenLop,
                              TBHK1 = (g.Where(r => r.hk.MaHK == 1).Sum(r => r.d.DiemCuoiHk)) / SoluongMH_HK1,
                              TBHK2 = (g.Where(r => r.hk.MaHK == 2).Sum(r => r.d.DiemCuoiHk)) / SoluongMH_HK2
                          };

                    //var db = this.FindResource("dshs") as DanhSachHocSinh;
                    //db.DSHS = lst.ToList();
                    // grdHocSinh.ItemsSource = null;
                    // grdHocSinh.ItemsSource = lst.ToList();


                }
                else
                {

                    lst = from hs in ctx.HocSinhs
                                                  .Where(h => h.TenHS.Contains(TenHocSinh))
                          from mh in ctx.MonHocs
                          from d in ctx.Diems
                          from hk in ctx.HocKies
                          from l in ctx.Lops
                          where hs.MaHS == d.MaHS
                          where d.MaMH == mh.MaMH
                          where mh.MaHK == hk.MaHK
                          where l.MaLop == hs.MaLop

                          group new { hs, mh, d, hk } by new { hs.MaHS, hs.TenHS, l.TenLop } into g

                          select new
                          {
                              TenHS = g.Key.TenHS,
                              TenLop = g.Key.TenLop,
                              TBHK1 = (g.Where(r => r.hk.MaHK == 1).Sum(r => r.d.DiemCuoiHk)) / SoluongMH_HK1,
                              TBHK2 = (g.Where(r => r.hk.MaHK == 2).Sum(r => r.d.DiemCuoiHk)) / SoluongMH_HK2
                          };

                    //var db = this.FindResource("dshs") as DanhSachHocSinh;
                    // db.DSHS = lst.ToList();
                    // grdHocSinh.ItemsSource = null;
                    // grdHocSinh.ItemsSource = lst.ToList();

                }

                return lst.ToList();
            }
        }

        public IEnumerable<Object> GetSearchQuery(string TenHocSinh)
        {
            List<HocSinh> hocsinh_New = new List<HocSinh>();
            using (QLHSEntities ctx = new QLHSEntities())
            {

                if (TenHocSinh.Length > 0)
                {
                    hocsinh_New = ctx.HocSinhs.Where(h => h.TenHS.Contains(TenHocSinh)).ToList();
                }

                IEnumerable<Object> lst = from hs in hocsinh_New
                                          from mh in ctx.MonHocs
                                          from d in ctx.Diems
                                          from hk in ctx.HocKies
                                          from l in ctx.Lops
                                          where hs.MaHS == d.MaHS
                                          where d.MaMH == mh.MaMH
                                          where mh.MaHK == hk.MaHK
                                          where l.MaLop == hs.MaLop
                                          group new { hs, mh, d, hk } by new { hs.MaHS, hs.TenHS, l.TenLop } into g

                                          select new
                                          {
                                              TenHS = g.Key.TenHS,
                                              TenLop = g.Key.TenLop,
                                              TBHK1 = (g.Where(r => r.hk.MaHK == 1).Sum(r => r.d.DiemCuoiHk)) / SoluongMH_HK1,
                                              TBHK2 = (g.Where(r => r.hk.MaHK == 2).Sum(r => r.d.DiemCuoiHk)) / SoluongMH_HK2
                                          };

                return lst;
            }
        }

        public int SoluongMonhoc_cuaHK(int maHK)
        {
            using(QLHSEntities ctx = new QLHSEntities())
            {
                int soluong = ctx.MonHocs.Where(m => m.MaHK == maHK).Count();
                return soluong;
            }
        }

        public List<HocSinh> layhstheolop(int a)
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                List<HocSinh> list = ctx.HocSinhs.Include("Lop").Include("Diems").Where(p => p.MaLop == a).ToList();
                return list;
            }
        }
        public int themhs(HocSinh a)
        {

            HocSinh p = new HocSinh
            {
                TenHS = a.TenHS,
                GioiTinh = a.GioiTinh,
                NgaySinh = a.NgaySinh,
                DiaChi = a.DiaChi,
                Email = a.Email,
                MaLop = a.MaLop
            };
            using (QLHSEntities ctx = new QLHSEntities())
            {
                Lop lop = ctx.Lops.Where(c => c.MaLop == a.MaLop).FirstOrDefault();
                lop.Siso += 1;
                ctx.HocSinhs.Add(p);
                ctx.SaveChanges();
                return 1;
            }
        }
        public int capnhaths(int mahs, HocSinh hs)
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                HocSinh hs1 = ctx.HocSinhs.Where(p => p.MaHS == mahs).FirstOrDefault();
                Lop lp = ctx.Lops.Where(p => p.MaLop == hs1.MaLop).FirstOrDefault();
                lp.Siso -= 1;
                Lop lp1 = ctx.Lops.Where(p => p.MaLop == hs.MaLop).FirstOrDefault();
                lp.Siso += 1;
                hs1.TenHS = hs.TenHS;
                hs1.GioiTinh = hs.GioiTinh;
                hs1.NgaySinh = hs.NgaySinh;
                hs1.DiaChi = hs.DiaChi;
                hs1.Email = hs.Email;
                hs1.MaLop = hs.MaLop;
                ctx.SaveChanges();
                return 1;
            }
        }
        public int ktdutuoi(HocSinh a)
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                Lop lop = ctx.Lops.Where(p => p.MaLop == a.MaLop).FirstOrDefault();
                Khoi khoi = ctx.Khois.Where(p => p.MaKhoi == lop.MaKhoi).FirstOrDefault();
                int nam = int.Parse(DateTime.Now.Year.ToString());
                int dtp = int.Parse(a.NgaySinh.Value.Year.ToString());
                int tuoi = nam - dtp;
                if (tuoi >= khoi.TuoiToiThieu && tuoi <= khoi.TuoiToiDa)
                    return 1;
                else
                    return 0;
            }
        }
        public void xoahs(int mahs)
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                HocSinh pro = ctx.HocSinhs.Where(p => p.MaHS == mahs).FirstOrDefault();
                Lop lop = ctx.Lops.Where(a => a.MaLop == pro.MaLop).FirstOrDefault();
                lop.Siso -= 1;
                if (pro != null)
                {
                    ctx.HocSinhs.Remove(pro);
                }
                ctx.SaveChanges();
            }
        }
        public HocSinh layhstheoma(int mahs)
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                HocSinh pro = ctx.HocSinhs.Where(p => p.MaHS == mahs).FirstOrDefault();
                return pro;
            }
        }

        public List<HocSinh> getAll_HS()
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                return ctx.HocSinhs.ToList();
            }
        }

    }
}
