﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class LopDAO
    {
        public List<Lop> getAll_Lop()
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                return  ctx.Lops.Include("HocSinhs").ToList();
            }
        }
        public int sisolop(int malop)
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                Lop lop = ctx.Lops.Where(p => p.MaLop == malop).FirstOrDefault();
                if (lop.Siso >= lop.SisoToiDa)
                    return 0;
                else
                    return 1;
            }
        }
        public int xoalop(int malop)
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                var pro = ctx.HocSinhs.Where(p => p.MaLop == malop).ToList();
                if (pro.Count() > 0)
                {
                    return 0;
                }
                else
                {
                    Lop lop = ctx.Lops.Where(a => a.MaLop == malop).FirstOrDefault();
                    Khoi k1 = ctx.Khois.Where(p => p.MaKhoi == lop.MaKhoi).FirstOrDefault();
                    k1.SL_Lop -= 1;
                    if (lop != null)
                    {
                        ctx.Lops.Remove(lop);
                    }
                    ctx.SaveChanges();
                    return 1;
                }

            }
        }
        public int themlop(Lop a)
        {
            Lop p = new Lop
            {
                TenLop = a.TenLop,
                SisoToiDa = a.SisoToiDa,
                Siso = 0,
                MaKhoi = a.MaKhoi
            };
            using (QLHSEntities ctx = new QLHSEntities())
            {
                Khoi khoi = ctx.Khois.Where(c => c.MaKhoi == a.MaKhoi).FirstOrDefault();
                khoi.SL_Lop += 1;
                ctx.Lops.Add(p);
                ctx.SaveChanges();
                return 1;
            }
        }
        public int sualop(Lop lp)
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                Lop lp1 = ctx.Lops.Where(p => p.MaLop == lp.MaLop).FirstOrDefault();
                Khoi k1 = ctx.Khois.Where(p => p.MaKhoi == lp1.MaKhoi).FirstOrDefault();
                k1.SL_Lop -= 1;
                Khoi k2 = ctx.Khois.Where(p => p.MaKhoi == lp.MaKhoi).FirstOrDefault();
                k1.SL_Lop += 1;
                lp1.TenLop = lp.TenLop;
                lp1.SisoToiDa = lp.SisoToiDa;
                lp1.MaKhoi = lp.MaKhoi;
                ctx.SaveChanges();
                return 1;
            }
        }

        
    }
}
