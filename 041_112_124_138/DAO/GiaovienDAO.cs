﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class GiaovienDAO
    {
        public ObservableCollection<GiaoVien> get_All_GiaoVien()
        {
            using(QLHSEntities ctx = new QLHSEntities())
            {
                ObservableCollection<GiaoVien> lst = new ObservableCollection<GiaoVien>(ctx.GiaoViens.ToList());
                return lst;
            }
        }

        public string addGiaoVien(GiaoVien gv)
        {
            using(QLHSEntities ctx = new QLHSEntities())
            {
                try
                {
                    string i = string.Empty;
                    ctx.GiaoViens.Add(gv);
                    ctx.SaveChanges();
                    return i;
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }
        }

        public void editGiaoVien(GiaoVien gv)
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                GiaoVien gvnew = ctx.GiaoViens.Where(g => g.MaGV == gv.MaGV).FirstOrDefault();
                gvnew.TenGV = gv.TenGV;
                gvnew.PassWord = gv.PassWord;
                gvnew.PhanQuyen = gv.PhanQuyen;
                ctx.SaveChanges();
            }
        }

        public void XoaGiaoVien(int magv)
        {
            using (QLHSEntities ctx = new QLHSEntities())
            {
                GiaoVien gv = ctx.GiaoViens.Where(g => g.MaGV == magv).FirstOrDefault();
                ctx.GiaoViens.Remove(gv);
                ctx.SaveChanges();
            }
        }

        public int kiemtraDN(string username, string pass)
        {
            using(QLHSEntities ctx = new QLHSEntities())
            {
                GiaoVien gv = ctx.GiaoViens.Where(g=>g.UserName == username && g.PassWord == pass).FirstOrDefault();
                if(gv != null)
                {
                    return Convert.ToInt32(gv.PhanQuyen);
                }
                return -1;
            }
        }

    }
}
