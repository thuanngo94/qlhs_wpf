USE [master]
GO
/****** Object:  Database [QLHS]    Script Date: 06/21/2015 11:37:29 ******/
CREATE DATABASE [QLHS] ON  PRIMARY 
( NAME = N'QLHS', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\QLHS.mdf' , SIZE = 2304KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'QLHS_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESS\MSSQL\DATA\QLHS_log.LDF' , SIZE = 768KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [QLHS] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QLHS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QLHS] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [QLHS] SET ANSI_NULLS OFF
GO
ALTER DATABASE [QLHS] SET ANSI_PADDING OFF
GO
ALTER DATABASE [QLHS] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [QLHS] SET ARITHABORT OFF
GO
ALTER DATABASE [QLHS] SET AUTO_CLOSE ON
GO
ALTER DATABASE [QLHS] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [QLHS] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [QLHS] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [QLHS] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [QLHS] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [QLHS] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [QLHS] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [QLHS] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [QLHS] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [QLHS] SET  ENABLE_BROKER
GO
ALTER DATABASE [QLHS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [QLHS] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [QLHS] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [QLHS] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [QLHS] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [QLHS] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [QLHS] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [QLHS] SET  READ_WRITE
GO
ALTER DATABASE [QLHS] SET RECOVERY SIMPLE
GO
ALTER DATABASE [QLHS] SET  MULTI_USER
GO
ALTER DATABASE [QLHS] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [QLHS] SET DB_CHAINING OFF
GO
USE [QLHS]
GO
/****** Object:  Table [dbo].[Khoi]    Script Date: 06/21/2015 11:37:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Khoi](
	[MaKhoi] [int] IDENTITY(1,1) NOT NULL,
	[TenKhoi] [nvarchar](50) NULL,
	[SL_Lop] [int] NULL,
	[TuoiToiThieu] [int] NULL,
	[TuoiToiDa] [int] NULL,
	[SL_LopToiDa] [int] NULL,
 CONSTRAINT [PK_Khoi] PRIMARY KEY CLUSTERED 
(
	[MaKhoi] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Khoi] ON
INSERT [dbo].[Khoi] ([MaKhoi], [TenKhoi], [SL_Lop], [TuoiToiThieu], [TuoiToiDa], [SL_LopToiDa]) VALUES (10, N'Khoi 10', 4, 16, 18, 40)
INSERT [dbo].[Khoi] ([MaKhoi], [TenKhoi], [SL_Lop], [TuoiToiThieu], [TuoiToiDa], [SL_LopToiDa]) VALUES (11, N'Khoi 11', 3, 17, 19, 40)
INSERT [dbo].[Khoi] ([MaKhoi], [TenKhoi], [SL_Lop], [TuoiToiThieu], [TuoiToiDa], [SL_LopToiDa]) VALUES (12, N'Khoi 12', 2, 18, 20, 40)
SET IDENTITY_INSERT [dbo].[Khoi] OFF
/****** Object:  Table [dbo].[HocKy]    Script Date: 06/21/2015 11:37:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HocKy](
	[MaHK] [int] IDENTITY(1,1) NOT NULL,
	[TenHK] [nvarchar](50) NULL,
	[SL_MonHoc] [int] NULL,
	[SL_MonHocMax] [int] NULL,
 CONSTRAINT [PK_HocKy] PRIMARY KEY CLUSTERED 
(
	[MaHK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[HocKy] ON
INSERT [dbo].[HocKy] ([MaHK], [TenHK], [SL_MonHoc], [SL_MonHocMax]) VALUES (1, N'Học Kì 1', 9, 9)
INSERT [dbo].[HocKy] ([MaHK], [TenHK], [SL_MonHoc], [SL_MonHocMax]) VALUES (2, N'Học Kì 2', 9, 9)
SET IDENTITY_INSERT [dbo].[HocKy] OFF
/****** Object:  Table [dbo].[GiaoVien]    Script Date: 06/21/2015 11:37:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GiaoVien](
	[MaGV] [int] IDENTITY(1,1) NOT NULL,
	[TenGV] [nvarchar](50) NULL,
	[UserName] [varchar](50) NULL,
	[PassWord] [varchar](50) NULL,
	[PhanQuyen] [int] NULL,
 CONSTRAINT [PK_GiaoVien] PRIMARY KEY CLUSTERED 
(
	[MaGV] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[GiaoVien] ON
INSERT [dbo].[GiaoVien] ([MaGV], [TenGV], [UserName], [PassWord], [PhanQuyen]) VALUES (1, N'Ngọc Tú', N'gv1', N'0CC175B9C0F1B6A831C399E269772661', 2)
INSERT [dbo].[GiaoVien] ([MaGV], [TenGV], [UserName], [PassWord], [PhanQuyen]) VALUES (2, N'Minh Anh', N'gv2', N'0CC175B9C0F1B6A831C399E269772661', 1)
INSERT [dbo].[GiaoVien] ([MaGV], [TenGV], [UserName], [PassWord], [PhanQuyen]) VALUES (3, N'Anh Khoa', N'gv3', N'0CC175B9C0F1B6A831C399E269772661', 1)
INSERT [dbo].[GiaoVien] ([MaGV], [TenGV], [UserName], [PassWord], [PhanQuyen]) VALUES (4, N'An Khương', N'gv4', N'0CC175B9C0F1B6A831C399E269772661', 1)
INSERT [dbo].[GiaoVien] ([MaGV], [TenGV], [UserName], [PassWord], [PhanQuyen]) VALUES (5, N'Mỹ Tiên', N'gv5', N'0CC175B9C0F1B6A831C399E269772661', 1)
INSERT [dbo].[GiaoVien] ([MaGV], [TenGV], [UserName], [PassWord], [PhanQuyen]) VALUES (6, N'Hương Giang', N'gv6', N'0CC175B9C0F1B6A831C399E269772661', 1)
INSERT [dbo].[GiaoVien] ([MaGV], [TenGV], [UserName], [PassWord], [PhanQuyen]) VALUES (7, N'An Nhiên', N'gv7', N'0CC175B9C0F1B6A831C399E269772661', 1)
INSERT [dbo].[GiaoVien] ([MaGV], [TenGV], [UserName], [PassWord], [PhanQuyen]) VALUES (8, N'Vinh Thao', N'gv8', N'0CC175B9C0F1B6A831C399E269772661', 1)
INSERT [dbo].[GiaoVien] ([MaGV], [TenGV], [UserName], [PassWord], [PhanQuyen]) VALUES (9, N'Kim Bình', N'gv9', N'0CC175B9C0F1B6A831C399E269772661', 1)
SET IDENTITY_INSERT [dbo].[GiaoVien] OFF
/****** Object:  Table [dbo].[MonHoc]    Script Date: 06/21/2015 11:37:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MonHoc](
	[MaMH] [int] IDENTITY(1,1) NOT NULL,
	[TenMH] [nvarchar](50) NULL,
	[DiemChuan] [float] NULL,
	[MaHK] [int] NULL,
 CONSTRAINT [PK_MonHoc] PRIMARY KEY CLUSTERED 
(
	[MaMH] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[MonHoc] ON
INSERT [dbo].[MonHoc] ([MaMH], [TenMH], [DiemChuan], [MaHK]) VALUES (1, N'Toán', 5, 1)
INSERT [dbo].[MonHoc] ([MaMH], [TenMH], [DiemChuan], [MaHK]) VALUES (2, N'Lý', 5, 1)
INSERT [dbo].[MonHoc] ([MaMH], [TenMH], [DiemChuan], [MaHK]) VALUES (3, N'Hóa', 5, 1)
INSERT [dbo].[MonHoc] ([MaMH], [TenMH], [DiemChuan], [MaHK]) VALUES (4, N'Sinh', 5, 1)
INSERT [dbo].[MonHoc] ([MaMH], [TenMH], [DiemChuan], [MaHK]) VALUES (5, N'Sử', 5, 1)
INSERT [dbo].[MonHoc] ([MaMH], [TenMH], [DiemChuan], [MaHK]) VALUES (6, N'Địa', 5, 1)
INSERT [dbo].[MonHoc] ([MaMH], [TenMH], [DiemChuan], [MaHK]) VALUES (7, N'Văn', 5, 1)
INSERT [dbo].[MonHoc] ([MaMH], [TenMH], [DiemChuan], [MaHK]) VALUES (8, N'Đạo Đức', 5, 1)
INSERT [dbo].[MonHoc] ([MaMH], [TenMH], [DiemChuan], [MaHK]) VALUES (9, N'Thể Dục', 5, 1)
INSERT [dbo].[MonHoc] ([MaMH], [TenMH], [DiemChuan], [MaHK]) VALUES (10, N'Toán', 5, 2)
INSERT [dbo].[MonHoc] ([MaMH], [TenMH], [DiemChuan], [MaHK]) VALUES (11, N'Lý', 5, 2)
INSERT [dbo].[MonHoc] ([MaMH], [TenMH], [DiemChuan], [MaHK]) VALUES (12, N'Hóa', 5, 2)
INSERT [dbo].[MonHoc] ([MaMH], [TenMH], [DiemChuan], [MaHK]) VALUES (13, N'Sinh', 5, 2)
INSERT [dbo].[MonHoc] ([MaMH], [TenMH], [DiemChuan], [MaHK]) VALUES (14, N'Sử', 5, 2)
INSERT [dbo].[MonHoc] ([MaMH], [TenMH], [DiemChuan], [MaHK]) VALUES (15, N'Địa', 5, 2)
INSERT [dbo].[MonHoc] ([MaMH], [TenMH], [DiemChuan], [MaHK]) VALUES (16, N'Văn', 5, 2)
INSERT [dbo].[MonHoc] ([MaMH], [TenMH], [DiemChuan], [MaHK]) VALUES (17, N'Đạo Đức', 5, 2)
INSERT [dbo].[MonHoc] ([MaMH], [TenMH], [DiemChuan], [MaHK]) VALUES (18, N'The Duc', 5, 2)
SET IDENTITY_INSERT [dbo].[MonHoc] OFF
/****** Object:  Table [dbo].[Lop]    Script Date: 06/21/2015 11:37:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lop](
	[MaLop] [int] IDENTITY(1,1) NOT NULL,
	[TenLop] [nvarchar](50) NULL,
	[Siso] [int] NULL,
	[SisoToiDa] [int] NULL,
	[MaKhoi] [int] NULL,
 CONSTRAINT [PK_Lop] PRIMARY KEY CLUSTERED 
(
	[MaLop] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Lop] ON
INSERT [dbo].[Lop] ([MaLop], [TenLop], [Siso], [SisoToiDa], [MaKhoi]) VALUES (101, N'10A1', 5, 40, 10)
INSERT [dbo].[Lop] ([MaLop], [TenLop], [Siso], [SisoToiDa], [MaKhoi]) VALUES (102, N'10A2', 5, 40, 10)
INSERT [dbo].[Lop] ([MaLop], [TenLop], [Siso], [SisoToiDa], [MaKhoi]) VALUES (103, N'10A3', 5, 40, 10)
INSERT [dbo].[Lop] ([MaLop], [TenLop], [Siso], [SisoToiDa], [MaKhoi]) VALUES (104, N'10A4', 5, 40, 10)
INSERT [dbo].[Lop] ([MaLop], [TenLop], [Siso], [SisoToiDa], [MaKhoi]) VALUES (111, N'11A1', 5, 40, 11)
INSERT [dbo].[Lop] ([MaLop], [TenLop], [Siso], [SisoToiDa], [MaKhoi]) VALUES (112, N'11A2', 5, 40, 11)
INSERT [dbo].[Lop] ([MaLop], [TenLop], [Siso], [SisoToiDa], [MaKhoi]) VALUES (113, N'11A3', 5, 40, 11)
INSERT [dbo].[Lop] ([MaLop], [TenLop], [Siso], [SisoToiDa], [MaKhoi]) VALUES (121, N'12A1', 5, 40, 12)
INSERT [dbo].[Lop] ([MaLop], [TenLop], [Siso], [SisoToiDa], [MaKhoi]) VALUES (122, N'12A2', 5, 40, 12)
SET IDENTITY_INSERT [dbo].[Lop] OFF
/****** Object:  Table [dbo].[HocSinh]    Script Date: 06/21/2015 11:37:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HocSinh](
	[MaHS] [int] IDENTITY(1,1) NOT NULL,
	[TenHS] [nvarchar](50) NULL,
	[GioiTinh] [bit] NULL,
	[NgaySinh] [date] NULL,
	[DiaChi] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[MaLop] [int] NULL,
 CONSTRAINT [PK_HocSinh] PRIMARY KEY CLUSTERED 
(
	[MaHS] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[HocSinh] ON
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (2, N'Ngô Minh Thuận', 1, CAST(0x07240B00 AS Date), N'Bình Tân', N'thuanngo94@gmail.com', 101)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (3, N'Chung Tuấn Tài', 1, CAST(0x08240B00 AS Date), N'Q.6', N'tuantai@gmail.com', 101)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (4, N'Hoàng Văn Thông', 1, CAST(0x09240B00 AS Date), N'Bình Tân', N'hoangthong@gmail.com', 101)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (5, N'Hoàng Văn Phúc', 1, CAST(0x0A240B00 AS Date), N'Nhà Bè', N'phuc@gmail.com', 101)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (6, N'Phạm Anh Phú', 1, CAST(0x0B240B00 AS Date), N'Phú Nhuận', N'quhck@gmail.com', 101)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (7, N'Huỳnh Thúy Quỳnh', 1, CAST(0x0C240B00 AS Date), N'Thủ Đức', N'qth@gmail.com', 102)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (8, N'Nguyễn Thùy Linh', 1, CAST(0x0D240B00 AS Date), N'Q.6', N'linhng@gmail.com', 102)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (9, N'Hoàng Thị Kim Ngọc', 1, CAST(0x0E240B00 AS Date), N'Q.7', N'ngoc@gmail.com', 102)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (10, N'Nguyễn Quang Thức', 1, CAST(0x0F240B00 AS Date), N'Q.5', N'thucqng@gmail.com', 102)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (11, N'Trương Hữu Hiếu', 1, CAST(0x10240B00 AS Date), N'Q.5', N'hieu@gmail.com', 102)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (12, N'Võ Hoàng Vũ', 1, CAST(0x11240B00 AS Date), N'Thủ Đức', N'vuck@gmail.com', 103)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (13, N'Huỳnh Minh Phụng', 1, CAST(0x12240B00 AS Date), N'Q.6', N'phungh@gmail.com', 103)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (14, N'Nguyễn Tố Linh', 1, CAST(0x13240B00 AS Date), N'Q.5', N'dkm@gmail.com', 103)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (15, N'Nguyễn Minh Tuấn', 1, CAST(0x14240B00 AS Date), N'Q.9', N'tuanvcl@gmail.com', 103)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (16, N'Lý Thái Minh', 1, CAST(0x15240B00 AS Date), N'Q.6', N'lyminh@gmail.com', 103)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (17, N'Nguyễn Thái Hoàng', 1, CAST(0x16240B00 AS Date), N'Q.6', N'hoang@gmail.com', 104)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (18, N'Tô Nhật Lang', 0, CAST(0x17240B00 AS Date), N'Q.5', N'tolang@gmail.com', 104)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (19, N'Châu Lộc', 0, CAST(0x18240B00 AS Date), N'Q.1', N'choulou@gmail.com', 104)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (20, N'Nguyễn Minh Thùy', 0, CAST(0x19240B00 AS Date), N'Gò Vấp', N'thuycasse@gmail.com', 104)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (21, N'Huỳnh Trương Thùy Dương', 0, CAST(0x19240B00 AS Date), N'Q.6', N'madamdu@gmail.com', 104)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (22, N'Cam Thị Hoàng Nguyên', 0, CAST(0xAC220B00 AS Date), N'Q.5', N'nguyencam@gmail.com', 111)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (23, N'Nguyễn Hoàng Yến', 0, CAST(0xAC220B00 AS Date), N'Q.6', N'yenvenus@gmail.com', 111)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (24, N'Vũ Thị Hiền', 0, CAST(0xAC220B00 AS Date), N'Vĩnh Lộc B', N'hienunu@gmail.com', 111)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (25, N'Trịnh Hảo Trân', 0, CAST(0xAC220B00 AS Date), N'Bình Tân', N'trantrinh@gmail.com', 111)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (26, N'Vũ Thị Hương', 0, CAST(0xAC220B00 AS Date), N'Q.6', N'huongheo@gmail.com', 111)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (27, N'Lương Hoàng Vũ', 0, CAST(0xAC220B00 AS Date), N'Q.6', N'vulu@gmail.com', 112)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (28, N'Nguyễn Hoàng Nam', 0, CAST(0xAC220B00 AS Date), N'Q.6', N'namml@gmail.com', 112)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (29, N'Nguyễn Yển Nhi', 0, CAST(0xAC220B00 AS Date), N'Phú Nhuận', N'nhiyen@gmail.com', 112)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (30, N'Hàng Trần Anh Tài', 0, CAST(0xAC220B00 AS Date), N'Q.6', N'taihang@gmail.com', 112)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (31, N'Nguyễn Văn Tài', 0, CAST(0xAC220B00 AS Date), N'Q.6', N'utcuom@gmail.com', 112)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (32, N'Nguyễn Thúy An', 1, CAST(0xAC220B00 AS Date), N'Phú Nhuận', N'anmodel@gmail.com', 113)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (33, N'Phạm Thị Giàu', 1, CAST(0xB9220B00 AS Date), N'Q.11', N'giaumini@gmail.com', 113)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (34, N'Nguyễn Thanh Bằng', 1, CAST(0xB9220B00 AS Date), N'Q.11', N'cobang@gmail.com', 113)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (35, N'Ngô Thị Tuyết', 1, CAST(0xB9220B00 AS Date), N'Binh Tân', N'tuyetngo@gmail.com', 113)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (36, N'Trần Kim Loan', 1, CAST(0xB9220B00 AS Date), N'Q.6', N'loankim@gmail.com', 113)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (37, N'Lê Kim Huy', 1, CAST(0x4C210B00 AS Date), N'q.4', N'huyme@gmail.com', 121)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (38, N'Nguyễn Bảo Châu', 1, CAST(0x4C210B00 AS Date), N'Q.6', N'chausumo@gmail.com', 121)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (39, N'Nguyễn Tuấn Anh', 1, CAST(0x4C210B00 AS Date), N'Vĩnh Lộc', N'tuananh@gmail.com', 121)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (40, N'Nguyễn Hoàng Phúc', 1, CAST(0x4C210B00 AS Date), N'Bình Chánh', N'phucbo@gmail.com', 121)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (41, N'Trần Quang Bảo', 1, CAST(0x4C210B00 AS Date), N'Phú Nhuận', N'baobao@gmail.com', 121)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (42, N'Tô Thiên Thanh', 1, CAST(0x4C210B00 AS Date), N'Q.6', N'thanhprincess@gmail.com', 122)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (43, N'Lê Văn Dương', 1, CAST(0x4C210B00 AS Date), N'Gò Vấp', N'leduong@gmail.com', 122)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (44, N'Nguyễn Thị Thúy Vi', 0, CAST(0x4C210B00 AS Date), N'Tân Hòa Đông', N'hannavi@gmail.com', 122)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (45, N'Lê Tài Đức', 0, CAST(0x4C210B00 AS Date), N'Q.6', N'ducle@gmail.com', 122)
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [GioiTinh], [NgaySinh], [DiaChi], [Email], [MaLop]) VALUES (46, N'Nguyễn Hữu Khang', 0, CAST(0x4C210B00 AS Date), N'Q.3', N'huukhang@gmail.com', 122)
SET IDENTITY_INSERT [dbo].[HocSinh] OFF
/****** Object:  Table [dbo].[ChiTietPhanCong]    Script Date: 06/21/2015 11:37:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietPhanCong](
	[MaLop] [int] NOT NULL,
	[MaMH] [int] NOT NULL,
	[MaGV] [int] NOT NULL,
 CONSTRAINT [PK_ChiTietPhanCong] PRIMARY KEY CLUSTERED 
(
	[MaLop] ASC,
	[MaMH] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[ChiTietPhanCong] ([MaLop], [MaMH], [MaGV]) VALUES (101, 1, 1)
INSERT [dbo].[ChiTietPhanCong] ([MaLop], [MaMH], [MaGV]) VALUES (101, 2, 3)
INSERT [dbo].[ChiTietPhanCong] ([MaLop], [MaMH], [MaGV]) VALUES (102, 1, 2)
INSERT [dbo].[ChiTietPhanCong] ([MaLop], [MaMH], [MaGV]) VALUES (102, 3, 5)
INSERT [dbo].[ChiTietPhanCong] ([MaLop], [MaMH], [MaGV]) VALUES (103, 1, 4)
INSERT [dbo].[ChiTietPhanCong] ([MaLop], [MaMH], [MaGV]) VALUES (104, 2, 3)
INSERT [dbo].[ChiTietPhanCong] ([MaLop], [MaMH], [MaGV]) VALUES (111, 4, 4)
INSERT [dbo].[ChiTietPhanCong] ([MaLop], [MaMH], [MaGV]) VALUES (111, 8, 8)
INSERT [dbo].[ChiTietPhanCong] ([MaLop], [MaMH], [MaGV]) VALUES (112, 8, 8)
INSERT [dbo].[ChiTietPhanCong] ([MaLop], [MaMH], [MaGV]) VALUES (113, 7, 2)
INSERT [dbo].[ChiTietPhanCong] ([MaLop], [MaMH], [MaGV]) VALUES (121, 8, 8)
INSERT [dbo].[ChiTietPhanCong] ([MaLop], [MaMH], [MaGV]) VALUES (122, 1, 1)
INSERT [dbo].[ChiTietPhanCong] ([MaLop], [MaMH], [MaGV]) VALUES (122, 7, 4)
/****** Object:  Table [dbo].[Diem]    Script Date: 06/21/2015 11:37:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Diem](
	[MaHS] [int] NOT NULL,
	[MaMH] [int] NOT NULL,
	[Diẹm15] [float] NULL,
	[Diem1tiet] [float] NULL,
	[DiemCuoiHk] [float] NULL,
 CONSTRAINT [PK_Diem] PRIMARY KEY CLUSTERED 
(
	[MaHS] ASC,
	[MaMH] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (2, 1, 5, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (2, 2, 5, 5, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (2, 3, 6, 6, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (2, 4, 7, 7, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (2, 5, 6, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (2, 6, 5, 5, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (2, 7, 7, 6, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (2, 8, 5, 6, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (2, 9, 7, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (2, 10, 5, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (2, 11, 7, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (2, 12, 5, 5, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (2, 13, 6, 6, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (2, 14, 7, 7, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (2, 15, 6, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (2, 16, 5, 5, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (2, 17, 7, 6, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (2, 18, 5, 6, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (3, 1, 7, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (3, 2, 6, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (3, 3, 10, 10, 10)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (3, 4, 9, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (3, 5, 6, 6, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (3, 6, 7, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (3, 7, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (3, 8, 7, 9, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (3, 9, 7, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (3, 10, 7, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (3, 11, 7, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (3, 12, 6, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (3, 13, 10, 10, 10)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (3, 14, 9, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (3, 15, 6, 6, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (3, 16, 7, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (3, 17, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (3, 18, 7, 9, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (4, 1, 5, 6, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (4, 2, 4, 5, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (4, 3, 6, 7, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (4, 4, 4, 4, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (4, 5, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (4, 6, 5, 8, 10)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (4, 7, 8, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (4, 8, 5, 4, 3)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (4, 9, 5, 4, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (4, 10, 5, 6, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (4, 11, 5, 4, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (4, 12, 4, 5, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (4, 13, 6, 7, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (4, 14, 4, 4, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (4, 15, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (4, 16, 5, 8, 10)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (4, 17, 8, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (4, 18, 5, 4, 3)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (5, 1, 6, 6, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (5, 2, 7, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (5, 3, 6, 6, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (5, 4, 4, 4, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (5, 5, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (5, 6, 5, 2, 1)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (5, 7, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (5, 8, 4, 4, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (5, 9, 8, 9, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (5, 10, 6, 6, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (5, 11, 8, 9, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (5, 12, 7, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (5, 13, 6, 6, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (5, 14, 4, 4, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (5, 15, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (5, 16, 5, 2, 1)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (5, 17, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (5, 18, 4, 4, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (6, 1, 3, 3, 3)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (6, 2, 7, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (6, 3, 4, 5, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (6, 4, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (6, 5, 9, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (6, 6, 7, 9, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (6, 7, 6, 8, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (6, 8, 7, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (6, 9, 4, 6, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (6, 10, 3, 3, 3)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (6, 12, 7, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (6, 13, 4, 5, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (6, 14, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (6, 15, 9, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (6, 16, 7, 9, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (6, 17, 6, 8, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (6, 18, 7, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (7, 1, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (7, 2, 7, 6, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (7, 3, 9, 5, 1)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (7, 4, 8, 9, 0)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (7, 5, 0, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (7, 6, 8, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (7, 7, 7, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (7, 8, 9, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (7, 9, 6, 6, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (7, 10, 6, 6, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (7, 11, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (7, 12, 7, 6, 6)
GO
print 'Processed 100 total records'
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (7, 13, 9, 5, 1)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (7, 14, 8, 9, 0)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (7, 15, 0, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (7, 16, 8, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (7, 17, 7, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (7, 18, 9, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (8, 1, 7, 6, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (8, 2, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (8, 3, 4, 4, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (8, 4, 4, 4, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (8, 5, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (8, 6, 7, 6, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (8, 7, 6, 8, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (8, 8, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (8, 9, 0, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (8, 10, 0, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (8, 11, 7, 6, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (8, 12, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (8, 13, 4, 4, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (8, 14, 4, 4, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (8, 15, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (8, 16, 7, 6, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (8, 17, 6, 8, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (8, 18, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (9, 1, 1, 1, 1)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (9, 2, 7, 8, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (9, 3, 7, 6, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (9, 4, 0, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (9, 5, 6, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (9, 6, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (9, 7, 5, 4, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (9, 8, 4, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (9, 9, 9, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (9, 10, 9, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (9, 11, 1, 1, 1)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (9, 12, 7, 8, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (9, 13, 7, 6, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (9, 14, 0, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (9, 15, 6, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (9, 16, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (9, 17, 5, 4, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (9, 18, 4, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (10, 1, 7, 7, 0)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (10, 2, 1, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (10, 3, 4, 8, 1)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (10, 4, 7, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (10, 5, 7, 8, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (10, 6, 8, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (10, 7, 6, 8, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (10, 8, 6, 1, 0)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (10, 9, 9, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (10, 10, 9, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (10, 11, 7, 7, 0)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (10, 12, 1, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (10, 13, 4, 8, 1)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (10, 14, 7, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (10, 15, 7, 8, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (10, 16, 8, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (10, 17, 6, 8, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (10, 18, 6, 1, 0)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (11, 1, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (11, 2, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (11, 3, 7, 8, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (11, 4, 7, 8, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (11, 5, 7, 9, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (11, 6, 7, 9, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (11, 7, 7, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (11, 8, 7, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (11, 9, 6, 7, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (11, 10, 6, 7, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (11, 11, 7, 8, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (11, 12, 7, 8, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (11, 13, 5, 5, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (11, 14, 5, 5, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (11, 15, 6, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (11, 16, 9, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (11, 17, 7, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (11, 18, 6, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (12, 1, 6, 7, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (12, 2, 7, 6, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (12, 3, 7, 6, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (12, 4, 4, 4, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (12, 5, 5, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (12, 6, 5, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (12, 7, 6, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (12, 8, 9, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (12, 9, 7, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (12, 10, 9, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (12, 11, 6, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (12, 12, 8, 6, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (12, 13, 8, 6, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (12, 14, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (12, 15, 6, 7, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (12, 16, 7, 6, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (12, 17, 7, 6, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (12, 18, 4, 4, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (13, 1, 5, 6, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (13, 2, 5, 7, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (13, 3, 9, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (13, 4, 5, 8, 3)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (13, 5, 2, 5, 5)
GO
print 'Processed 200 total records'
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (13, 6, 5, 5, 2)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (13, 7, 7, 4, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (13, 8, 8, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (13, 9, 9, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (13, 10, 7, 8, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (13, 11, 4, 5, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (13, 12, 5, 1, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (13, 13, 8, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (13, 14, 9, 9, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (13, 15, 3, 6, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (13, 16, 2, 5, 2)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (13, 17, 6, 7, 1)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (13, 18, 8, 1, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (14, 1, 7, 8, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (14, 2, 9, 9, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (14, 3, 4, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (14, 4, 5, 8, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (14, 5, 5, 5, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (14, 6, 5, 8, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (14, 7, 6, 8, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (14, 8, 8, 6, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (14, 9, 9, 8, 2)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (14, 10, 7, 9, 1)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (14, 11, 8, 5, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (14, 12, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (14, 13, 7, 1, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (14, 14, 9, 2, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (14, 15, 6, 5, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (14, 16, 5, 9, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (14, 17, 8, 4, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (14, 18, 8, 7, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (15, 1, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (15, 2, 8, 6, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (15, 3, 5, 9, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (15, 4, 6, 8, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (15, 5, 9, 5, 1)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (15, 6, 7, 8, 2)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (15, 7, 8, 5, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (15, 8, 9, 9, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (15, 9, 8, 5, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (15, 10, 9, 7, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (15, 11, 9, 5, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (15, 12, 8, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (15, 13, 9, 4, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (15, 14, 9, 8, 1)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (15, 15, 7, 9, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (15, 16, 7, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (15, 17, 7, 5, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (15, 18, 8, 8, 1)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (16, 1, 8, 6, 2)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (16, 2, 9, 2, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (16, 3, 9, 2, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (16, 4, 6, 1, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (16, 5, 7, 4, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (16, 6, 5, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (16, 7, 6, 8, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (16, 8, 5, 6, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (16, 9, 5, 5, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (16, 10, 7, 5, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (16, 11, 8, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (16, 12, 8, 9, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (16, 13, 9, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (16, 14, 7, 4, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (16, 15, 7, 6, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (16, 16, 7, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (16, 17, 5, 4, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (16, 18, 5, 5, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (17, 1, 5, 6, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (17, 2, 5, 9, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (17, 3, 5, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (17, 4, 6, 7, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (17, 5, 7, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (17, 6, 8, 3, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (17, 7, 8, 6, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (17, 8, 9, 9, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (17, 9, 9, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (17, 10, 4, 5, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (17, 11, 6, 2, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (17, 12, 5, 1, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (17, 13, 8, 4, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (17, 14, 7, 7, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (17, 15, 8, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (17, 16, 9, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (17, 17, 8, 7, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (18, 1, 8, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (18, 2, 4, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (18, 3, 5, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (18, 4, 6, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (18, 5, 7, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (18, 6, 2, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (18, 7, 5, 6, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (18, 8, 6, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (18, 9, 7, 5, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (18, 10, 5, 2, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (18, 11, 4, 5, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (18, 12, 8, 4, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (18, 13, 9, 5, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (18, 14, 8, 6, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (18, 15, 6, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (18, 16, 5, 8, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (18, 17, 4, 8, 8)
GO
print 'Processed 300 total records'
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (18, 18, 5, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (19, 1, 8, 5, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (19, 2, 5, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (19, 3, 5, 8, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (19, 4, 6, 6, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (19, 5, 8, 2, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (19, 6, 9, 6, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (19, 7, 9, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (19, 8, 7, 4, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (19, 9, 4, 9, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (19, 10, 8, 9, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (19, 11, 5, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (19, 12, 8, 4, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (19, 13, 7, 5, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (19, 14, 9, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (19, 15, 6, 6, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (19, 16, 4, 4, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (19, 17, 8, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (19, 18, 9, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (20, 1, 7, 5, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (20, 2, 4, 9, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (20, 3, 5, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (20, 4, 8, 4, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (20, 5, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (20, 6, 9, 4, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (20, 7, 7, 2, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (20, 8, 9, 5, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (20, 9, 2, 6, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (20, 10, 5, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (20, 11, 3, 9, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (20, 12, 6, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (20, 13, 4, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (20, 14, 6, 8, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (20, 15, 9, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (20, 16, 7, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (20, 17, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (20, 18, 9, 8, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (21, 1, 4, 9, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (21, 2, 7, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (21, 3, 5, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (21, 4, 6, 5, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (21, 5, 8, 8, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (21, 6, 2, 6, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (21, 7, 5, 4, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (21, 8, 6, 4, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (21, 9, 7, 4, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (21, 10, 8, 4, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (21, 11, 9, 6, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (21, 12, 6, 4, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (21, 13, 5, 5, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (21, 14, 4, 5, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (21, 15, 4, 5, 2)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (21, 16, 5, 6, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (21, 17, 8, 5, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (21, 18, 9, 9, 1)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (22, 1, 7, 8, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (22, 2, 7, 47, 2)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (22, 3, 6, 2, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (22, 4, 6, 5, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (22, 5, 7, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (22, 6, 9, 7, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (22, 7, 9, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (22, 8, 9, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (22, 9, 9, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (22, 10, 9, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (22, 11, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (22, 12, 5, 6, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (22, 13, 5, 7, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (22, 14, 9, 8, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (22, 15, 7, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (22, 16, 9, 7, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (22, 17, 9, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (22, 18, 9, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (23, 1, 7, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (23, 2, 7, 6, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (23, 3, 6, 5, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (23, 4, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (23, 5, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (23, 6, 8, 9, 0)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (23, 7, 6, 8, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (23, 8, 7, 6, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (23, 9, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (23, 10, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (23, 11, 7, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (23, 12, 7, 6, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (23, 13, 6, 5, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (23, 14, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (23, 15, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (23, 16, 8, 9, 0)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (23, 17, 6, 8, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (23, 18, 7, 6, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (24, 1, 7, 6, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (24, 2, 7, 8, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (24, 3, 7, 6, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (24, 4, 7, 8, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (24, 5, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (24, 6, 7, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (24, 7, 6, 5, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (24, 8, 7, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (24, 9, 7, 6, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (24, 10, 7, 6, 5)
GO
print 'Processed 400 total records'
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (24, 11, 7, 6, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (24, 12, 7, 8, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (24, 13, 7, 6, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (24, 14, 7, 8, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (24, 15, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (24, 16, 7, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (24, 17, 6, 5, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (24, 18, 7, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (25, 1, 6, 6, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (25, 2, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (25, 3, 4, 4, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (25, 4, 6, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (25, 5, 6, 6, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (25, 6, 7, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (25, 7, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (25, 8, 9, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (25, 9, 7, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (25, 10, 7, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (25, 11, 6, 6, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (25, 12, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (25, 13, 4, 4, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (25, 14, 6, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (25, 15, 6, 6, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (25, 16, 7, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (25, 17, 8, 8, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (25, 18, 9, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (26, 1, 6, 6, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (26, 2, 6, 5, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (26, 3, 5, 4, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (26, 4, 6, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (26, 5, 6, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (26, 6, 5, 3, 1)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (26, 7, 6, 5, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (26, 8, 6, 9, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (26, 9, 8, 9, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (26, 10, 8, 9, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (26, 11, 6, 6, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (26, 12, 6, 5, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (26, 13, 5, 4, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (26, 14, 6, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (26, 15, 6, 7, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (26, 16, 5, 3, 1)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (26, 17, 6, 5, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (26, 18, 6, 9, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (27, 7, 8, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (27, 17, 8, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (28, 7, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (28, 17, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (29, 6, 7, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (29, 7, 6, 5, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (29, 16, 7, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (29, 17, 6, 5, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (30, 7, 8, 9, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (30, 17, 8, 9, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (31, 6, 7, 8, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (31, 16, 7, 8, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (32, 7, 6, 9, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (32, 17, 6, 9, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (33, 7, 6, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (33, 17, 6, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (34, 7, 8, 6, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (34, 17, 8, 6, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (35, 8, 7, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (35, 18, 7, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (36, 9, 9, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (36, 10, 9, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (37, 1, 4, 6, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (37, 2, 4, 6, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (37, 3, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (37, 4, 8, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (37, 5, 7, 6, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (37, 6, 8, 9, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (37, 7, 8, 6, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (37, 8, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (37, 9, 8, 9, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (37, 10, 8, 9, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (37, 11, 4, 6, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (37, 12, 4, 6, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (37, 13, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (37, 14, 8, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (37, 15, 7, 6, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (37, 16, 8, 9, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (37, 17, 8, 6, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (37, 18, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (38, 1, 8, 9, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (38, 2, 7, 6, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (38, 3, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (38, 4, 6, 5, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (38, 5, 5, 5, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (38, 6, 3, 6, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (38, 7, 9, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (38, 8, 6, 5, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (38, 9, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (38, 10, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (38, 11, 8, 9, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (38, 12, 7, 6, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (38, 13, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (38, 14, 6, 5, 4)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (38, 15, 5, 5, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (38, 16, 3, 6, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (38, 17, 9, 9, 9)
GO
print 'Processed 500 total records'
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (38, 18, 6, 5, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (39, 1, 7, 6, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (39, 2, 6, 7, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (39, 3, 7, 6, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (39, 4, 8, 6, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (39, 5, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (39, 6, 8, 8, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (39, 7, 8, 9, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (39, 8, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (39, 9, 9, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (39, 10, 9, 9, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (39, 11, 7, 6, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (39, 12, 6, 7, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (39, 13, 7, 6, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (39, 14, 8, 6, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (39, 15, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (39, 16, 8, 8, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (39, 17, 8, 9, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (39, 18, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (40, 1, 7, 0, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (40, 2, 7, 8, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (40, 3, 7, 6, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (40, 4, 6, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (40, 5, 7, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (40, 6, 8, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (40, 7, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (40, 8, 7, 6, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (40, 9, 7, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (40, 10, 7, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (40, 11, 7, 0, 9)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (40, 12, 7, 8, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (40, 13, 7, 6, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (40, 14, 6, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (40, 15, 7, 8, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (40, 16, 8, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (40, 17, 6, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (40, 18, 7, 6, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (41, 7, 8, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (41, 17, 8, 7, 6)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (42, 8, 7, 9, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (42, 18, 7, 9, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (43, 6, 5, 4, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (43, 16, 5, 4, 7)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (44, 6, 5, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (44, 16, 5, 7, 8)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (45, 9, 7, 6, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (45, 10, 7, 6, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (46, 7, 6, 5, 5)
INSERT [dbo].[Diem] ([MaHS], [MaMH], [Diẹm15], [Diem1tiet], [DiemCuoiHk]) VALUES (46, 17, 6, 5, 5)
/****** Object:  StoredProcedure [dbo].[Sp_baocaomon]    Script Date: 06/21/2015 11:37:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Sp_baocaomon]
      @maMon int,@maHk int
AS

BEGIN

      select  lop.TenLop, mh.TenMH,hk.TenHK,lop.Siso, COUNT(lop.MaLop) as SLdat, COUNT(lop.MaLop)*100/lop.Siso as Tyle
		from MonHoc mh, HocKy hk, Lop lop, Diem d, HocSinh hs 
		where mh.MaHK = hk.MaHK and mh.MaMH = d.MaMH and hs.MaLop = lop.MaLop and d.MaHS = hs.MaHS
		and d.DiemCuoiHk >=5
		and mh.MaMH = @maMon and hk.MaHK = @maHk
		group by lop.MaLop,lop.TenLop, mh.TenMH,hk.TenHK,lop.Siso

END
GO
/****** Object:  StoredProcedure [dbo].[Sp_baocaohocky]    Script Date: 06/21/2015 11:37:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Sp_baocaohocky]
      @maHk int
AS

BEGIN

      select  lop.TenLop,hk.TenHK,lop.Siso, COUNT(lop.MaLop) as SLdat, COUNT(lop.MaLop)*100/(lop.Siso * hk.SL_MonHoc) as Tyle
		from MonHoc mh, HocKy hk, Lop lop, Diem d, HocSinh hs 
		where mh.MaHK = hk.MaHK and mh.MaMH = d.MaMH and hs.MaLop = lop.MaLop and d.MaHS = hs.MaHS
		and d.DiemCuoiHk >=5
		and hk.MaHK = @maHk
		group by lop.MaLop,lop.TenLop,hk.TenHK,lop.Siso,hk.SL_MonHoc

END
GO
/****** Object:  ForeignKey [FK_MonHoc_HocKy1]    Script Date: 06/21/2015 11:37:30 ******/
ALTER TABLE [dbo].[MonHoc]  WITH CHECK ADD  CONSTRAINT [FK_MonHoc_HocKy1] FOREIGN KEY([MaHK])
REFERENCES [dbo].[HocKy] ([MaHK])
GO
ALTER TABLE [dbo].[MonHoc] CHECK CONSTRAINT [FK_MonHoc_HocKy1]
GO
/****** Object:  ForeignKey [FK_Lop_Khoi1]    Script Date: 06/21/2015 11:37:30 ******/
ALTER TABLE [dbo].[Lop]  WITH CHECK ADD  CONSTRAINT [FK_Lop_Khoi1] FOREIGN KEY([MaKhoi])
REFERENCES [dbo].[Khoi] ([MaKhoi])
GO
ALTER TABLE [dbo].[Lop] CHECK CONSTRAINT [FK_Lop_Khoi1]
GO
/****** Object:  ForeignKey [FK_HocSinh_Lop1]    Script Date: 06/21/2015 11:37:30 ******/
ALTER TABLE [dbo].[HocSinh]  WITH CHECK ADD  CONSTRAINT [FK_HocSinh_Lop1] FOREIGN KEY([MaLop])
REFERENCES [dbo].[Lop] ([MaLop])
GO
ALTER TABLE [dbo].[HocSinh] CHECK CONSTRAINT [FK_HocSinh_Lop1]
GO
/****** Object:  ForeignKey [FK_ChiTietPhanCong_GiaoVien]    Script Date: 06/21/2015 11:37:30 ******/
ALTER TABLE [dbo].[ChiTietPhanCong]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietPhanCong_GiaoVien] FOREIGN KEY([MaGV])
REFERENCES [dbo].[GiaoVien] ([MaGV])
GO
ALTER TABLE [dbo].[ChiTietPhanCong] CHECK CONSTRAINT [FK_ChiTietPhanCong_GiaoVien]
GO
/****** Object:  ForeignKey [FK_ChiTietPhanCong_Lop]    Script Date: 06/21/2015 11:37:30 ******/
ALTER TABLE [dbo].[ChiTietPhanCong]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietPhanCong_Lop] FOREIGN KEY([MaLop])
REFERENCES [dbo].[Lop] ([MaLop])
GO
ALTER TABLE [dbo].[ChiTietPhanCong] CHECK CONSTRAINT [FK_ChiTietPhanCong_Lop]
GO
/****** Object:  ForeignKey [FK_ChiTietPhanCong_MonHoc]    Script Date: 06/21/2015 11:37:30 ******/
ALTER TABLE [dbo].[ChiTietPhanCong]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietPhanCong_MonHoc] FOREIGN KEY([MaMH])
REFERENCES [dbo].[MonHoc] ([MaMH])
GO
ALTER TABLE [dbo].[ChiTietPhanCong] CHECK CONSTRAINT [FK_ChiTietPhanCong_MonHoc]
GO
/****** Object:  ForeignKey [FK_Diem_HocSinh]    Script Date: 06/21/2015 11:37:30 ******/
ALTER TABLE [dbo].[Diem]  WITH CHECK ADD  CONSTRAINT [FK_Diem_HocSinh] FOREIGN KEY([MaHS])
REFERENCES [dbo].[HocSinh] ([MaHS])
GO
ALTER TABLE [dbo].[Diem] CHECK CONSTRAINT [FK_Diem_HocSinh]
GO
/****** Object:  ForeignKey [FK_Diem_MonHoc1]    Script Date: 06/21/2015 11:37:30 ******/
ALTER TABLE [dbo].[Diem]  WITH CHECK ADD  CONSTRAINT [FK_Diem_MonHoc1] FOREIGN KEY([MaMH])
REFERENCES [dbo].[MonHoc] ([MaMH])
GO
ALTER TABLE [dbo].[Diem] CHECK CONSTRAINT [FK_Diem_MonHoc1]
GO
