﻿using DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
     public class HocKyBUS
    { 
         HocKyDAO dao = new HocKyDAO();

         public List<HocKy> get_ALL_HK()
         {
            
             return dao.get_ALL_HK();
         }
         public void addHK(HocKy hk)
         {
             dao.addHK(hk);
         }
         public void editHK(HocKy hk)
         {
             dao.editHK(hk);
         }
         public void delete(int mahk)
         {
             dao.deleteHk(mahk);
         }
         public HocKy getHocKy_byMaHk(int maHK)
         {
             return dao.getHocKy_byMaHk(maHK);
         }

     }
}
