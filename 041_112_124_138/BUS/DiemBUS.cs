﻿using System;
using DAO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class DiemBUS
    {
        BangDiemDAO dao = new BangDiemDAO();

        public List<Diem> getAll_Diem()
        {
            return dao.getAll_Diem();
        }
        public List<Diem> getMaLop_Diem(int malop, int mahk, int mamh)
        {
            return dao.getMaLop_Diem(malop, mahk,mamh);
        }

        public void addDiem(Diem d)
        {
            dao.addDiem(d);
        }
        public void EditDiem(Diem d)
        {
            dao.EditDiem(d);
        }
        public void DeleteDiem(Diem d)
        {
            dao.DeleteDiem(d);
        }

        public List<MonHoc> LayDS_Monhoc_ChuaNhapDiem_TheoHocKy_cuaHocSinh(int maHS, int maHK)
        {
            return dao.LayDS_Monhoc_ChuaNhapDiem_TheoHocKy_cuaHocSinh(maHS, maHK);
        }
    }
}
