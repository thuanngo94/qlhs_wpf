﻿using DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class LopBUS
    {
        public List<Lop> getAll_Lop()
        {
            LopDAO dao = new LopDAO();
            return dao.getAll_Lop();
        }
        public int xoalop(string malop)
        {
            int malop1 = Convert.ToInt32(malop);
            LopDAO dao = new LopDAO();
            return dao.xoalop(malop1);
        }
        public int themlop(Lop lp)
        {
            KhoiDAO khoi = new KhoiDAO();
            LopDAO lop = new LopDAO();
            int makhoi = Convert.ToInt32(lp.MaKhoi);
            int a = khoi.soluonglop(makhoi);
            if (a == 0)
                return 0;
            else
                return lop.themlop(lp);
        }
        public int sualop(Lop lp)
        {
            KhoiDAO khoi = new KhoiDAO();
            LopDAO lop = new LopDAO();
            int makhoi = Convert.ToInt32(lp.MaKhoi);
            int a = khoi.soluonglop(makhoi);
            if (a == 0)
                return 0;
            else
                return lop.sualop(lp);
        }
    }
}
