﻿using DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class HocSinhBUS
    {
        public IEnumerable<Object> getall_hs_DTB(string TenHocSinh, string malop1, int kiemtra)
        {
            HocSinhDAO dao = new HocSinhDAO();
            int malop = 1;
            if(malop1 != "")
            {
                malop = Convert.ToInt32(malop1);
            }
            
            return dao.getall_hs_DTB(TenHocSinh, malop, kiemtra);
        }

        public List<HocSinh> layhstheolop(int a)
        {
            HocSinhDAO dao = new HocSinhDAO();
            return dao.layhstheolop(a);
        }
        public void xoahs(string mahs)
        {
            int mahs1 = Convert.ToInt32(mahs);
            HocSinhDAO dao = new HocSinhDAO();
            dao.xoahs(mahs1);
        }
        public HocSinh layhstheoma(string mahs)
        {
            int mahs1 = Convert.ToInt32(mahs);
            HocSinhDAO dao = new HocSinhDAO();
            return dao.layhstheoma(mahs1);
        }
        public int themhocsinh(HocSinh hs)
        {
            LopDAO lop = new LopDAO();
            HocSinhDAO hocsinh = new HocSinhDAO();
            int malop = Convert.ToInt32(hs.MaLop);
            int a = lop.sisolop(malop);
            int b = hocsinh.ktdutuoi(hs);
            if (a == 0)
                return 0;
            else
            {
                if (b == 0)
                    return 2;
                else
                {
                    int c = hocsinh.themhs(hs);
                    return c;
                }
            }
        }
        public int suahocsinh(int mahs, HocSinh hs)
        {
            LopDAO lop = new LopDAO();
            HocSinhDAO hocsinh = new HocSinhDAO();
            int malop = Convert.ToInt32(hs.MaLop);
            int a = lop.sisolop(malop);
            int b = hocsinh.ktdutuoi(hs);
            if (a == 0)
                return 0;
            else
            {
                if (b == 0)
                    return 2;
                else
                {
                    int c = hocsinh.capnhaths(mahs, hs);
                    return c;
                }
            }
        }
        public List<HocSinh> getAll_HS()
        {
            HocSinhDAO dao = new HocSinhDAO();
            return dao.getAll_HS();
        }
    }
}
