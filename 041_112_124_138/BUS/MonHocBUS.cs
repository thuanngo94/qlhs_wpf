﻿using System;
using DAO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class MonHocBUS
    {
        MonHocDAO dao = new MonHocDAO();

        public List<MonHoc> get_ALL_MH()
        {
            return dao.get_ALL_MH();
        }
        public ICollection<MonHoc> get_ALL_MH_TheoMaHK(int Mahk)
        {
            return dao.get_ALL_MH_TheoMaHK(Mahk);
        }
        public void addMH(MonHoc mh)
        {
            dao.addMH(mh);
        }
        public void editMH(MonHoc mh)
        {
            dao.editMH(mh);
        }
        public void deleteMH(int mh)
        {
            dao.deleteMH(mh);
        }
        public String kiemtra_xoa(int maMH)
        {
            return dao.kiemtra_xoa(maMH);
        }

        public bool kiemtra_SoluongMH(int mahk)
        {
            return dao.kiemtra_SoluongMH(mahk);
        }
    }
}
