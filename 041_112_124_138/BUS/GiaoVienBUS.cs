﻿using DAO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class GiaoVienBUS
    {
        GiaovienDAO dao = new GiaovienDAO();

        public ObservableCollection<GiaoVien> get_All_GiaoVien()
        {
            return dao.get_All_GiaoVien();
        }

        public string addGiaoVien(GiaoVien gv)
        {
            string newpass = StringUtils.Md5(gv.PassWord);
            gv.PassWord = newpass;
            return dao.addGiaoVien(gv);
        }

        public void editGiaoVien(GiaoVien gv)
        {
            dao.editGiaoVien(gv);
        }

        public void XoaGiaoVien(int magv)
        {
            dao.XoaGiaoVien(magv);
        }
        public int kiemtraDN(string username, string pass)
        {
            string newpass = StringUtils.Md5(pass);
            return dao.kiemtraDN(username, newpass);
        }
    }
}
