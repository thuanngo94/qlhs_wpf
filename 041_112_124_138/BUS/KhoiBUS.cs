﻿using DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    public class KhoiBUS
    {
        public List<Khoi> getAll_Khoi()
        {
            KhoiDAO dao = new KhoiDAO();
            return dao.getAll_Khoi();
        }
        public int xoakhoi(string makhoi)
        {
            int makhoi1 = Convert.ToInt32(makhoi);
            KhoiDAO dao = new KhoiDAO();
            return dao.xoakhoi(makhoi1);
        }
        public int themkhoi(Khoi k1)
        {
            KhoiDAO khoi = new KhoiDAO();
            return khoi.themkhoi(k1);
        }
        public int suakhoi(Khoi k1)
        {
            KhoiDAO khoi = new KhoiDAO();
            return khoi.suakhoi(k1);
        }

    }
}
