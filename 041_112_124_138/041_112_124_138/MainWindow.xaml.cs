﻿using _041_112_124_138;
using _041_112_124_138.WD;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using DevExpress.Xpf.Core;
using System.Collections;
using System.Windows;
using System.Windows.Controls;

namespace _041_112_124_138
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow 
    {
        public DangNhap dangnhap;
        public int phanquyen = 0;
        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(MainWindow_Loaded);
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            //DXSplashScreen.Close();
            //this.Activate();
        }

        // Ham tao tap moi
        public void addnew(string strTabName, UserControl ucConTent)
        {
            foreach (DevExpress.Xpf.Core.DXTabItem tabpage in tabcontrol.Items)
            {
                if (tabpage.Tag == strTabName)
                {
                    tabcontrol.SelectedItem = tabpage;
                    return;
                }
            }


            DevExpress.Xpf.Core.DXTabItem tabitem = new DevExpress.Xpf.Core.DXTabItem();
            tabitem.Header = strTabName;

            //tabitem.AllowHide = DevExpress.Utils.DefaultBoolean.True;
            
            tabitem.Tag = strTabName;

            tabitem.Content = ucConTent;
            tabcontrol.Items.Add(tabitem);
            tabcontrol.SelectedItem = tabitem;
            tabcontrol.View.AllowHideTabItems = true;
            tabcontrol.View.RemoveTabItemsOnHiding = true;

        } 

        private void BarButtonItem_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            UC_TimKiem_HocSinh user = new UC_TimKiem_HocSinh();
            //UserControl1 user = new UserControl1();
            addnew("Tra Cứu Học Sinh", user);
        }
        private void btnTaiKhoan_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            UC_TaiKhoan user = new UC_TaiKhoan();
            //UserControl1 user = new UserControl1();
            addnew("Tài Khoản", user);
        }

        //private void btnSaoLuu_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        //{
        //    //WD_SaoLuu a = new WD_SaoLuu();
        //    //a.Show();
        //}

        private void btnHocSinh_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            UC_HocSinh user = new UC_HocSinh();
            addnew("Học Sinh", user);
        }

        private void btnLop_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            UC_LopHoc lt = new UC_LopHoc();
            addnew("Lớp Học", lt);
        }

        private void btnKhoi_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            UC_Khoi khoi = new UC_Khoi();
            addnew("Khối", khoi);
        }


        private void btnMonHoc_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            UC_MonHoc user = new UC_MonHoc();
            addnew("Môn Học", user);
        }

        private void btnHocKi_ItemClick_1(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            UC_HocKi user = new UC_HocKi();
            addnew("Học Kỳ", user);
        }

        private void btnDiem_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            UC_BangDiem user = new UC_BangDiem();
            addnew("Điểm", user);
        }

        private void btnBaoCao_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            //Instantiate variables
            XemBaoCaoMon baocao = new XemBaoCaoMon();
            baocao.ShowDialog();
            
        }

        private void btnBaoCaohocky_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            XemBaoCaoHK baocao = new XemBaoCaoHK();
            baocao.ShowDialog();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            dangnhap.Close();
            if(phanquyen == 0 )
            {
                TabAdmin.IsVisible = false;
                tabGiaoVien.IsVisible = false;
            }
            else if(phanquyen == 1)
            {
                TabAdmin.IsVisible = false;
            }
        }
    }
}
