﻿using _041_112_124_138.special_class;
using BUS;
using DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _041_112_124_138.WD
{
    /// <summary>
    /// Interaction logic for XemBaoCaoHK.xaml
    /// </summary>
    public partial class XemBaoCaoHK : Window
    {
        public XemBaoCaoHK()
        {
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            HocKyBUS hkbus = new HocKyBUS();
            List<HocKy> listHK = hkbus.get_ALL_HK();
            var db1 = this.FindResource("dshk") as DanhSachHocKi;
            db1.DSHK = listHK;

            cbHK.SelectedValuePath = "MonHocs";
            cbHK.DisplayMemberPath = "TenHK";
            cbHK.SelectedIndex = 0;
            MonHocBUS mhbus = new MonHocBUS();
        }
        private void btnXem_Click(object sender, RoutedEventArgs e)
        {
            HienBaoCao_TKMon baocao = new HienBaoCao_TKMon();
            HashSet<MonHoc> mh = (HashSet<MonHoc>)cbHK.SelectedValue;
            baocao.MaHK = Convert.ToInt32(mh.FirstOrDefault().MaHK);
            baocao.kiemtra = 1;
            baocao.ShowDialog();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
