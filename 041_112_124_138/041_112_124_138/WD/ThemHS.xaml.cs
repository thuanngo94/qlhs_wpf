﻿using BUS;
using DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _041_112_124_138
{
    /// <summary>
    /// Interaction logic for ThemHS.xaml
    /// </summary>
    public partial class ThemHS : Window
    {
        public string mahs = "";
        public int kt = 0;
        public ThemHS()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LopBUS bus = new LopBUS();
            cbxlop.DataContext = bus.getAll_Lop();
            cbxlop.SelectedValuePath = "MaLop";
            cbxlop.DisplayMemberPath = "TenLop";
            cbxlop.SelectedIndex = 0;

            if (kt == 1)
            {
                HocSinhBUS bus1 = new HocSinhBUS();
                HocSinh a = bus1.layhstheoma(mahs);
                txtten.Text = a.TenHS;
                txtemail.Text = a.Email;
                txtdiachi.Text = a.DiaChi;
                int magt = Convert.ToByte(a.GioiTinh);
                cbxgt.SelectedValue = magt;

                cbxlop.SelectedValue = a.MaLop;
                datens.Text = a.NgaySinh.ToString();
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (kt == 0)
            {
                HocSinhBUS bus = new HocSinhBUS();
                HocSinh hs = new HocSinh();
                if (txtten.Text == "" || txtemail.Text == "" || txtdiachi.Text == "" || datens.Text == "")
                    MessageBox.Show(" Vui Lòng Nhập Đủ Thông Tin");
                else
                {
                    int gt = Convert.ToInt32(cbxgt.SelectedValue);
                    hs.GioiTinh = Convert.ToBoolean(gt);
                    hs.Email = txtemail.Text;
                    hs.DiaChi = txtdiachi.Text;
                    hs.TenHS = txtten.Text;
                    hs.MaLop = Convert.ToInt32(cbxlop.SelectedValue);
                    hs.NgaySinh = Convert.ToDateTime(datens.Text);
                    int a = bus.themhocsinh(hs);
                    if (a == 0)
                        MessageBox.Show("Lớp này đã đủ số lượng học sinh");
                    else
                    {
                        if (a == 2)
                            MessageBox.Show("Học Sinh Sai Tuổi Quy Định");
                        else
                            MessageBox.Show("Thêm Học Sinh Thành Công");
                    }
                }
            }
            else
            {
                HocSinhBUS bus = new HocSinhBUS();
                HocSinh hs = new HocSinh();
                if (txtten.Text == "" || txtemail.Text == "" || txtdiachi.Text == "" || datens.Text == "")
                    MessageBox.Show(" Vui Lòng Không Để Trống");
                else
                {
                    int gt = Convert.ToInt32(cbxgt.SelectedValue);
                    hs.GioiTinh = Convert.ToBoolean(gt);
                    hs.Email = txtemail.Text;
                    hs.DiaChi = txtdiachi.Text;
                    hs.TenHS = txtten.Text;
                    hs.MaLop = Convert.ToInt32(cbxlop.SelectedValue);
                    hs.NgaySinh = Convert.ToDateTime(datens.Text);
                    int mahs1 = Convert.ToInt32(mahs);
                    int a = bus.suahocsinh(mahs1, hs);
                    if (a == 0)
                        MessageBox.Show("Lớp này đã đủ số lượng học sinh");
                    else
                    {
                        if (a == 2)
                            MessageBox.Show("Học Sinh Sai Tuổi Quy Định");
                        else
                        {
                            MessageBox.Show("Sửa Học Thành Công");
                            this.Close();
                        }
                           
                    }
                }
            }
        }

        private void btncancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
