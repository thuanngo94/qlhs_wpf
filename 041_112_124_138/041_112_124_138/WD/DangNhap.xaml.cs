﻿using BUS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _041_112_124_138
{
    /// <summary>
    /// Interaction logic for DangNhap.xaml
    /// </summary>
    public partial class DangNhap : Window
    {
        public DangNhap()
        {
            InitializeComponent();
        }
        public DangNhap dangnhap;
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            GiaoVienBUS bus = new GiaoVienBUS();
            int phanquyen = bus.kiemtraDN(txtTenDN.Text, txtMK.Password);
            if(txtMK.Password == "" && txtTenDN.Text =="")
            {
                MainWindow mw = new MainWindow();
                mw.dangnhap = this;
                this.Hide();
                mw.Show();
            }
            else
            {
                if (phanquyen != -1)
                {
                    MainWindow mw = new MainWindow();
                    mw.dangnhap = this;
                    mw.phanquyen = phanquyen;
                    this.Hide();
                    mw.Show();
                }
                else
                {
                    MessageBox.Show("Tên đăng nhập hoặc mật khẩu không đúng");
                }
            }
            

        }

        private void btnthoat_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
