﻿using DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _041_112_124_138
{
    /// <summary>
    /// Interaction logic for WD_Them_TaiKhoan.xaml
    /// </summary>
    public partial class WD_Them_TaiKhoan : Window
    {
         public GiaoVien gv = new GiaoVien();
         public int kiemtra_T_S = 0;
        public WD_Them_TaiKhoan()
        {
            InitializeComponent();
        }
        GiaovienDAO dao = new GiaovienDAO();

        private void btnLuu_Click(object sender, RoutedEventArgs e)
        {
            if(kiemtra_T_S == 0)
            {
                bool kiemtra = KiemtraInput();

                if (kiemtra == true)
                {
                    GiaoVien gvnew = new GiaoVien();
                    gvnew.TenGV = txtTenGV.Text;
                    gvnew.PassWord = StringUtils.Md5(txtMatKhau.Password);
                    gvnew.PhanQuyen = Convert.ToInt32(cbPhanQuyen.SelectedValue.ToString());
                    gvnew.UserName = txtUser.Text;
                    dao.addGiaoVien(gvnew);
                    MessageBox.Show("Thêm thành công");
                }
            }
            else
            {
                bool kiemtra = KiemtraInput();
                if (kiemtra == true)
                {
                    GiaoVien gvnew = new GiaoVien();
                    gvnew.MaGV = gv.MaGV;
                    gvnew.TenGV = txtTenGV.Text;
                    gvnew.PassWord = StringUtils.Md5(txtMatKhau.Password);
                    gvnew.PhanQuyen = Convert.ToInt32(cbPhanQuyen.SelectedValue.ToString());
                    dao.editGiaoVien(gvnew);
                    MessageBox.Show("Cập nhật thành công");
                    this.Close();
                }
            }
            
        }

        private bool KiemtraInput()
        {
            bool kiemtra = true;
            if (txtUser.Text == string.Empty)
            {
                MessageBox.Show("Tên đăng nhập không được để trống");
                kiemtra = false;
            }
            else if (txtMatKhau.Password == string.Empty)
            {
                MessageBox.Show("Mật khẩu không được để trống");
                kiemtra = false;
            }
            else if (txtNhaplaiMK.Password == "")
            {
                MessageBox.Show("Mật khẩu không được để trống");
                kiemtra = false;
            }

            else if (txtMatKhau.Password != txtNhaplaiMK.Password)
            {
                MessageBox.Show("Mật khẩu và mật khẩu nhập lại không đúng");
                kiemtra = false;
            }

            else if (txtTenGV.Text == string.Empty)
            {
                MessageBox.Show("Tên giáo viên không được để trống");
                kiemtra = false;
            }
            return kiemtra;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if(kiemtra_T_S == 1)
            {
                txtUser.Text = gv.UserName;
                txtUser.IsEnabled = false;
                lbMatKhau.Text = "Mật khẩu mới là: ";
                txtTenGV.Text = gv.TenGV;
                //txtMatKhau.Password = gv.PassWord;
                cbPhanQuyen.SelectedValue = gv.PhanQuyen;
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
