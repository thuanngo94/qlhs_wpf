﻿using BUS;
using DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _041_112_124_138
{
    /// <summary>
    /// Interaction logic for ThemKhoi.xaml
    /// </summary>
    public partial class ThemKhoi : Window
    {
        public int kt = 0;
        public Khoi k2 = new Khoi();
        public ThemKhoi()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if ( kt == 1)
            {
                txtten.Text = k2.TenKhoi;
                txttuoitoithieu.Text = k2.TuoiToiThieu.ToString();
                txttuoitoida.Text = k2.TuoiToiDa.ToString();
                txtslloptoida.Text = k2.SL_LopToiDa.ToString();
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (kt == 0)
            {
                KhoiBUS bus = new KhoiBUS();
                Khoi k1 = new Khoi();
                if (txtten.Text == "" || txttuoitoithieu.Text == "" || txttuoitoida.Text == "" ||txtslloptoida.Text == "")
                    MessageBox.Show("Nhập đủ thông tin");
                else
                {
                    if (Convert.ToInt32(txttuoitoithieu.Text) > Convert.ToInt32(txttuoitoida.Text))
                        MessageBox.Show("Tuổi Tối Đa Ko Được Nhỏ Hơn Tuổi Tối Thiểu");
                    else
                    {
                        k1.TenKhoi = txtten.Text;
                        k1.TuoiToiThieu = Convert.ToInt32(txttuoitoithieu.Text);
                        k1.TuoiToiDa = Convert.ToInt32(txttuoitoida.Text);
                        k1.SL_LopToiDa = Convert.ToInt32(txtslloptoida.Text);
                        int a = bus.themkhoi(k1);
                        MessageBox.Show("Thêm khối thành công");
                    }
                    
                }
            }
            else
            {
                KhoiBUS bus = new KhoiBUS();
                Khoi k1 = new Khoi();
                if (txtten.Text == "" || txttuoitoithieu.Text == "" || txttuoitoida.Text == "" || txtslloptoida.Text == "")
                    MessageBox.Show("Thông Tin Không Để Trống");
                else
                {
                    if (Convert.ToInt32(txttuoitoithieu.Text) > Convert.ToInt32(txttuoitoida.Text))
                        MessageBox.Show("Tuổi Tối Đa Ko Được Nhỏ Hơn Tuổi Tối Thiểu");
                    else
                    {
                        k1.MaKhoi = k2.MaKhoi;
                        k1.TenKhoi = txtten.Text;
                        k1.TuoiToiThieu = Convert.ToInt32(txttuoitoithieu.Text);
                        k1.TuoiToiDa = Convert.ToInt32(txttuoitoida.Text);
                        k1.SL_LopToiDa = Convert.ToInt32(txtslloptoida.Text);
                        int a = bus.suakhoi(k1);
                        if (a == 0)
                            MessageBox.Show("SL Lớp tối đa nhỏ hơn số lớp hiện có ");
                        else
                        {
                            MessageBox.Show("Cập nhật thành công");
                            this.Close();
                        }
                    }        
                }
            }
        }

        private void btncancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }

        private void txttuoitoida_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private void txttuoitoithieu_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private void txtslloptoida_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }
    }
}
