﻿using BUS;
using DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _041_112_124_138
{
    /// <summary>
    /// Interaction logic for ThemLop.xaml
    /// </summary>
    public partial class ThemLop : Window
    {
        public Lop lp = new Lop();
        public int kt = 0;
        public ThemLop()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            KhoiBUS bus = new KhoiBUS();
            cbxtenkhoi.DataContext = bus.getAll_Khoi();
            cbxtenkhoi.SelectedValuePath = "MaKhoi";
            cbxtenkhoi.DisplayMemberPath = "TenKhoi";
            cbxtenkhoi.SelectedIndex = 0;

            if (kt == 1)
            {
                cbxtenkhoi.IsEnabled = false;
                txtten.Text = lp.TenLop.ToString();
                txtsisotoida.Text = lp.SisoToiDa.ToString();
                cbxtenkhoi.SelectedValue = lp.MaKhoi;
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (kt == 0)
            {
                LopBUS bus = new LopBUS();
                Lop lop1 = new Lop();
                if (txtten.Text == "" || txtsisotoida.Text == "")
                    MessageBox.Show("Nhập đủ thông tin");
                else
                {
                    lop1.TenLop = txtten.Text;
                    lop1.SisoToiDa = Convert.ToInt32(txtsisotoida.Text);
                    lop1.MaKhoi = Convert.ToInt32(cbxtenkhoi.SelectedValue);
                    int a = bus.themlop(lop1);
                    if (a == 0)
                        MessageBox.Show("Khối đã có đủ lớp");
                    else
                        MessageBox.Show("Thêm lớp thành công");
                }
            }
            else
            {
                LopBUS bus = new LopBUS();
                Lop lop1 = new Lop();
                if (txtten.Text == "" || txtsisotoida.Text == "")
                    MessageBox.Show("Nhập đủ thông tin");
                else
                {
                    lop1.MaLop = lp.MaLop;
                    lop1.TenLop = txtten.Text;
                    lop1.SisoToiDa = Convert.ToInt32(txtsisotoida.Text);
                    lop1.MaKhoi = Convert.ToInt32(cbxtenkhoi.SelectedValue);
                    int a = bus.sualop(lop1);
                    if (a == 0)
                        MessageBox.Show("Khối đã có đủ lớp");
                    else
                    {
                        MessageBox.Show("Cập nhật thành công");
                        this.Close();
                    }
                        
                }

            }

        }

        private void btncancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void txtsisotoida_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }
     
    }
}
