﻿using _041_112_124_138.special_class;
using BUS;
using DAO;
using DevExpress.Xpf.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _041_112_124_138
{
    /// <summary>
    /// Interaction logic for ThemDiem.xaml
    /// </summary>
    public partial class ThemDiem : Window
    {
        DiemBUS bus = new DiemBUS();
        public int maHS = 0;
        public double diem15 = 0;
        public double diem1tiet = 0;
        public double diemcuoiky = 0;

        public int malop = -1;
        public int mahk = -1;
        public int mamh = -1;


        public int kiemtra = 0;
        public ThemDiem()
        {
            InitializeComponent();
            
           
        }
        
        private void btnThem_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            if(kiemtra == 0)
            {
                Diem d = new Diem();
                d.MaHS = Convert.ToInt32(cbHS.SelectedValue.ToString());
               
                if (txtDiemCK.Text != "" || txtDiem1tiet.Text != "" || txtDiemCK.Text != "")
                {
                    if (cbMH.SelectedValue != null)
                    {
                        d.MaMH = Convert.ToInt32(cbMH.SelectedValue.ToString());
                        if (Convert.ToDouble(txtDiemCK.Text) <= 10 && Convert.ToDouble(txtDiem1tiet.Text) <= 10 && Convert.ToDouble(txtDiem15.Text) <= 10)
                        {
                            d.DiemCuoiHk = Convert.ToDouble(txtDiemCK.Text);
                            d.Diem1tiet = Convert.ToDouble(txtDiem1tiet.Text);
                            d.Diẹm15 = Convert.ToDouble(txtDiem15.Text);
                            bus.addDiem(d);
                            MessageBox.Show("Thêm điểm thành công");
                            Close();
                        }
                        else
                        {
                            MessageBox.Show("Điểm số chỉ được nhập từ 0 tới 10");
                        }

                    }
                    else
                    {
                        MessageBox.Show("Bạn chưa chọn môn học hoặc môn học không chọn được");
                    }

                }
                else
                {
                    MessageBox.Show("Nhập chưa đủ thông tin");
                }
            }
            else
            {
                Diem d = new Diem();
                d.MaHS = maHS;
                d.MaMH = mamh;
                if (txtDiemCK.Text != "" || txtDiem1tiet.Text != "" || txtDiemCK.Text != "")
                {
                    if (cbMH.SelectedValue != null)
                    {
                        if (Convert.ToDouble(txtDiemCK.Text) <= 10 && Convert.ToDouble(txtDiem1tiet.Text) <= 10 && Convert.ToDouble(txtDiem15.Text) <= 10)
                        {
                            d.DiemCuoiHk = Convert.ToDouble(txtDiemCK.Text);
                            d.Diem1tiet = Convert.ToDouble(txtDiem1tiet.Text);
                            d.Diẹm15 = Convert.ToDouble(txtDiem15.Text);
                            bus.EditDiem(d);
                            MessageBox.Show("Sửa điểm thành công");
                            Close();
                        }
                        else
                        {
                            MessageBox.Show("Điểm số chỉ được nhập từ 0 tới 10");
                        }

                    }
                    else
                    {
                        MessageBox.Show("Bạn chưa chọn môn học hoặc môn học không chọn được");
                    }

                }
                else
                {
                    MessageBox.Show("Nhập chưa đủ thông tin");
                }
            }
            
        }

        private void btnThoat_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            Close();
        }
        private void txtDiem15_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            double value = 0;
            if (!double.TryParse(txtDiem15.Text + e.Text.ToString(), out value))
            {
                e.Handled = true;
            }
            
        }

        private void txtDiem1tiet_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            double value = 0;
            if (!double.TryParse(txtDiem1tiet.Text + e.Text.ToString(), out value))
            {
                e.Handled = true;
            }
        }

        private void txtDiemCK_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            double value = 0;
            if (!double.TryParse(txtDiemCK.Text + e.Text.ToString(), out value))
            {
                e.Handled = true;
            }
        }
        
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LopBUS lopbus = new LopBUS();
            MonHocBUS mhBus = new MonHocBUS();
            HocSinhBUS hsbus = new HocSinhBUS();
            HocKyBUS hkBus = new HocKyBUS();

            cbHS.DataContext = hsbus.getAll_HS();
            cbHS.SelectedValuePath = "MaHS";
            cbHS.DisplayMemberPath = "TenHS";



            cbHK.DataContext = hkBus.get_ALL_HK();
            cbHK.SelectedValuePath = "MaHK";
            cbHK.DisplayMemberPath = "TenHK";
            

            if(kiemtra == 0)
            {
                cbHS.SelectedIndex = 0;
                cbHK.SelectedIndex = 0;
                List<MonHoc> lst = bus.LayDS_Monhoc_ChuaNhapDiem_TheoHocKy_cuaHocSinh(Convert.ToInt32(cbHS.SelectedValue), Convert.ToInt32(cbHK.SelectedValue));
                cbMH.DataContext = lst;
                if (lst.Count != 0)
                {
                    cbMH.SelectedIndex = 0;

                }
            }
            else
            {
                cbHS.IsEnabled = false;
                cbHK.IsEnabled = false;
                cbMH.IsEnabled = false;
                cbHS.SelectedValue = maHS;
                cbHK.SelectedValue = mahk;
                List<MonHoc> lst = mhBus.get_ALL_MH();
                cbMH.ItemsSource = lst;
                cbMH.SelectedValuePath = "MaMH";
                cbMH.DisplayMemberPath = "TenMH";
                cbMH.SelectedValue = mamh;

               

                txtDiem15.Text = diem15.ToString();
                txtDiem1tiet.Text = diem1tiet.ToString();
                txtDiemCK.Text = diemcuoiky.ToString();
                wd.Title = "CẬp nhật điểm";
            }
            
            
           
           

        }

        private void cbHK_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            List<MonHoc> lst1 = bus.LayDS_Monhoc_ChuaNhapDiem_TheoHocKy_cuaHocSinh(Convert.ToInt32(cbHS.SelectedValue), Convert.ToInt32(cbHK.SelectedValue));
            cbMH.DataContext = null;
            if (lst1.Count != 0)
            {
                cbMH.DataContext = lst1;
                cbMH.SelectedItem = cbMH.Items.GetItemAt(0);
            }
            
        }

        private void cbHS_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            List<MonHoc> lst1 = bus.LayDS_Monhoc_ChuaNhapDiem_TheoHocKy_cuaHocSinh(Convert.ToInt32(cbHS.SelectedValue), Convert.ToInt32(cbHK.SelectedValue));
            cbMH.DataContext = null;
            if (lst1.Count != 0)
            {
                cbMH.DataContext = lst1;
                cbMH.SelectedItem = cbMH.Items.GetItemAt(0);
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if(cbMH.SelectedValue != null && cbHK.SelectedValue != null)
            {
                HocSinhBUS bus = new HocSinhBUS();
                String mahs1 = cbHS.SelectedValue.ToString();
                HocSinh hs = bus.layhstheoma(mahs1);
                malop = Convert.ToInt32(hs.MaLop);
                mamh = Convert.ToInt32(cbMH.SelectedValue.ToString());
                mahk = Convert.ToInt32(cbHK.SelectedValue.ToString()) - 1;   
            }
           
        }
    }
}
