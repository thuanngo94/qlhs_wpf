﻿using BUS;
using DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _041_112_124_138
{
    /// <summary>
    /// Interaction logic for Them_HocKi.xaml
    /// </summary>
    public partial class Them_HocKi : Window
    {
        public HocKy hocky = new HocKy();
        public int kiemtra = 0;
        public Them_HocKi()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (kiemtra == 1)
            {
                txtTenHK.Text = hocky.TenHK.ToString();
                txtSLMH.Text = hocky.SL_MonHocMax.ToString();
            }
            //txtMaHK.Text = hocky.MaHK.ToString();

            // txtSLMHM.Text = hocky.SL_MonHocMax.ToString();

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        HocKyBUS bus = new HocKyBUS();
        private void btnLuu_Click(object sender, RoutedEventArgs e)
        {
            if (kiemtra == 0)
            {
                HocKy hk = new HocKy();
                if (txtSLMH.Text == "" || txtTenHK.Text == "")
                {
                    MessageBox.Show("Chưa đủ thông tin");
                }
                else
                {
                    hk.TenHK = txtTenHK.Text;
                    hk.SL_MonHoc = Convert.ToInt32(txtSLMH.Text);
                    bus.addHK(hk);
                }

            }
            else if (kiemtra == 1)
            {
                if (txtSLMH.Text == "" || txtTenHK.Text == "")
                {
                    MessageBox.Show("Chưa đủ thông tin");
                }
                else
                {
                    HocKy hk = new HocKy();
                    hk.MaHK = hocky.MaHK;
                    hk.TenHK = txtTenHK.Text;
                    hk.SL_MonHocMax = Convert.ToInt32(txtSLMH.Text);
                    bus.editHK(hk);
                    this.Close();
                }

            }


        }

        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }

        private void txtSLMH_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }


    }
}
