﻿using BUS;
using DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _041_112_124_138
{
    /// <summary>
    /// Interaction logic for Them_MonHoc.xaml
    /// </summary>
    public partial class Them_MonHoc : Window
    {
        public MonHoc monhoc = new MonHoc();
        public int kiemtra = 0;
        public Them_MonHoc()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            HocKyBUS hkbus = new HocKyBUS();
            cbHocKy.DataContext = hkbus.get_ALL_HK();
            cbHocKy.SelectedValuePath = "MaHK";
            cbHocKy.DisplayMemberPath = "TenHK";
            cbHocKy.SelectedIndex = 0;
            if (kiemtra == 1)
            {

                txtDiem.Text = monhoc.DiemChuan.ToString();
                txtTenMH.Text = monhoc.TenMH.ToString();
                cbHocKy.SelectedValue = monhoc.MaHK;
                cbHocKy.IsEnabled = false;
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        MonHocBUS bus = new MonHocBUS();

        private void btnLuu_Click(object sender, RoutedEventArgs e)
        {
            MonHoc mh = new MonHoc();
            if (kiemtra == 0)
            {

                if (txtTenMH.Text == "" || txtDiem.Text == "")
                {
                    MessageBox.Show("Nhập chưa đủ thông tin");
                }
                else if (Convert.ToDouble(txtDiem.Text) > 10)
                {
                    MessageBox.Show("Điểm chuẩn phải từ 0 -> 10");
                }
                else
                {

                    mh.TenMH = txtTenMH.Text;
                    mh.DiemChuan = Convert.ToDouble(txtDiem.Text);
                    mh.MaHK = Convert.ToInt32(cbHocKy.SelectedValue);
                    if (bus.kiemtra_SoluongMH(Convert.ToInt32(mh.MaHK)) == true)
                    {
                        bus.addMH(mh);
                        MessageBox.Show("Thêm Môn Học Thành Công");
                    }
                    else
                    {
                        MessageBox.Show("Học kỳ này đã đủ số lượng nên không thể thêm");
                    }

                }
            }
            else
            {
                if (txtTenMH.Text == "" || txtDiem.Text == "")
                {
                    MessageBox.Show("Nhập chưa đủ thông tin");
                }
                else if (Convert.ToDouble(txtDiem.Text) > 10)
                {
                    MessageBox.Show("Điểm chuẩn phải từ 0 -> 10");
                }
                else
                {
                    mh.MaMH = monhoc.MaMH;
                    mh.TenMH = txtTenMH.Text;
                    mh.MaHK = Convert.ToInt32(cbHocKy.SelectedValue);
                    mh.DiemChuan = Convert.ToDouble(txtDiem.Text);


                    bus.editMH(mh);
                    MessageBox.Show("Sửa môn Học Thành Công");

                }



            }
        }
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("^[.][0-9]+$|^[0-9]*[.]{0,1}[0-9]*$"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }

        private void txtDiem_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            //e.Handled = !IsTextAllowed(e.Text);
            double value = 0;
            if (!double.TryParse(txtDiem.Text + e.Text.ToString(), out value))
            {
                e.Handled = true;
            }
        }


    }
}
