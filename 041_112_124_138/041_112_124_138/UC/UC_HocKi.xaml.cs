﻿using _041_112_124_138.special_class;
using BUS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _041_112_124_138
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class UC_HocKi : UserControl
    {
        public UC_HocKi()
        {
            InitializeComponent();
            LoadHocki();
        }
        HocKyBUS bus = new HocKyBUS();

        public void LoadHocki()
        {
            //using (QLHSEntities ctx = new QLHSEntities())
            //{
            //    List<HocKy> lst = ctx.HocKies.Include("MonHocs").ToList();

            var db = this.FindResource("dshk") as DanhSachHocKi;
            db.DSHK = bus.get_ALL_HK();
            //}
        }

        private void btnThem_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            Them_HocKi HK = new Them_HocKi();
            HK.ShowDialog();
            var db = this.FindResource("dshk") as DanhSachHocKi;
            db.DSHK = bus.get_ALL_HK();

        }

        
       

        private void grdHocKi_SelectedItemChanged(object sender, DevExpress.Xpf.Grid.SelectedItemChangedEventArgs e)
        {

        }

        private void btnSua_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            Them_HocKi HK = new Them_HocKi();
            HK.hocky.MaHK = Convert.ToInt32(grdHocKi.GetFocusedRowCellValue("MaHK").ToString());
            HK.hocky.TenHK =grdHocKi.GetFocusedRowCellValue("TenHK").ToString();
            HK.hocky.SL_MonHocMax = Convert.ToInt32(grdHocKi.GetFocusedRowCellValue("SL_MonHocMax").ToString());
            HK.kiemtra = 1;
            HK.ShowDialog();

            var db = this.FindResource("dshk") as DanhSachHocKi;
            db.DSHK = bus.get_ALL_HK();
        }

        private void btnXoa_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            int mahk;
            mahk = Convert.ToInt32(grdHocKi.GetFocusedRowCellValue("MaHK").ToString());
            bus.delete(mahk);

            var db = this.FindResource("dshk") as DanhSachHocKi;
            db.DSHK = bus.get_ALL_HK();
        }
    }
}
