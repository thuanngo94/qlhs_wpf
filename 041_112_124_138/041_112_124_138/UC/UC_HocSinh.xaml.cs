﻿using _041_112_124_138.special_class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BUS;
using DAO;
namespace _041_112_124_138
{
    /// <summary>
    /// Interaction logic for UC_HocSinh.xaml
    /// </summary>
    public partial class UC_HocSinh : UserControl
    {
        public UC_HocSinh()
        {
            InitializeComponent();
            Load_data_hocSinh();
        }
        LopBUS bus = new LopBUS();
        public void LoadDSHS()
        {
            
            cbLop.DataContext = bus.getAll_Lop();
            cbLop.SelectedValuePath = "MaLop";
            cbLop.DisplayMemberPath = "TenLop";
            cbLop.SelectedIndex = 0;
            var db = this.FindResource("dslop") as DSLop;
            db.DSLOP = bus.getAll_Lop(); 
        }

        private void btnThem_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            ThemHS themhs = new ThemHS();
            themhs.kt = 0;
            themhs.ShowDialog();

            Load_data_hocSinh();
            //HocSinhBUS bus = new HocSinhBUS();
            //var db = this.FindResource("dshs") as DSHocSinh;
            //int malop = Convert.ToInt32(cbLop.SelectedValue);
            //List<HocSinh> lst = bus.layhstheolop(malop);
            //db.DSHS = lst;
            //txtSiso.Text = lst.Count().ToString();
        }

        private void Load_data_hocSinh()
        {
            cbLop.DataContext = null;
            cbLop.DataContext = bus.getAll_Lop();
            var db = this.FindResource("dslop") as DSLop;
            db.DSLOP = bus.getAll_Lop();
            cbLop.SelectedValuePath = "MaLop";
            cbLop.DisplayMemberPath = "TenLop";
            cbLop.SelectedIndex = 0;
            
        }

        private void cbLop_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //HocSinhBUS bus = new HocSinhBUS();
            //var db = this.FindResource("dshs") as DSHocSinh;
            //int malop = Convert.ToInt32(cbLop.SelectedValue);
            //List<HocSinh> lst = bus.layhstheolop(malop);
            //db.DSHS = lst;
            //txtSiso.Text = lst.Count().ToString();
        }

        private void btnSua_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            ThemHS themhs = new ThemHS();
            themhs.mahs = grdHocSinh.GetFocusedRowCellValue("MaHS").ToString();
            themhs.kt = 1;
            themhs.ShowDialog();
            Load_data_hocSinh();
        }

        private void grdHocSinh_SelectedItemChanged(object sender, DevExpress.Xpf.Grid.SelectedItemChangedEventArgs e)
        {
            btnSua.IsVisible = true;
            btnXoa.IsVisible = true;
        }

        private void btnXoa_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            HocSinhBUS bus = new HocSinhBUS();
            bus.xoahs(grdHocSinh.GetFocusedRowCellValue("MaHS").ToString());
            MessageBox.Show("Xóa Học Sinh Thành Công");
            Load_data_hocSinh();
        }

       
    }
}
