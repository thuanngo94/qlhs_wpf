﻿using _041_112_124_138.special_class;
using BUS;
using DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _041_112_124_138
{
    /// <summary>
    /// Interaction logic for UC_LopHoc.xaml
    /// </summary>
    public partial class UC_LopHoc : UserControl
    {
        public UC_LopHoc()
        {
            InitializeComponent();
            Load();
        }
        public void Load()
        {
            LopBUS bus = new LopBUS();
            var db = this.FindResource("dslop") as DSLop;
            List<Lop> list = bus.getAll_Lop();
            db.DSLOP = list;
        }
        private void btnThem_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            ThemLop lop = new ThemLop();
            lop.kt = 0;
            lop.ShowDialog();
            Load();
        }

        private void btnSua_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            ThemLop lop = new ThemLop();
            lop.kt = 1;
            lop.lp.MaLop = Convert.ToInt32(grdlop.GetFocusedRowCellValue("MaLop").ToString());
            lop.lp.TenLop = grdlop.GetFocusedRowCellValue("TenLop").ToString();
            lop.lp.SisoToiDa = Convert.ToInt32(grdlop.GetFocusedRowCellValue("SisoToiDa").ToString());
            lop.lp.MaKhoi = Convert.ToInt32(grdlop.GetFocusedRowCellValue("MaKhoi").ToString());
            lop.ShowDialog();
            Load();
        }

        private void btnXoa_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            LopBUS bus = new LopBUS();
            int a = bus.xoalop(grdlop.GetFocusedRowCellValue("MaLop").ToString());
            if( a == 0)
            {
                MessageBox.Show("Lớp có học sinh ko xóa dc");
            }
            else
            {
                MessageBox.Show(" Xóa Lớp Thành Công");
                Load();
            }
        }
    }
}
