﻿using System;
using BUS;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using _041_112_124_138.special_class;

namespace _041_112_124_138
{
    /// <summary>
    /// Interaction logic for UC_MonHoc.xaml
    /// </summary>
    public partial class UC_MonHoc : UserControl
    {
        public UC_MonHoc()
        {
            InitializeComponent();

        }

        public void LoadMonHoc()
        {
            //using (QLHSEntities ctx = new QLHSEntities())
            //{
            //    List<HocKy> lst = ctx.HocKies.Include("MonHocs").ToList();
            MonHocBUS bus = new MonHocBUS();

            var db = this.FindResource("DSMH") as DanhSachMonHoc;
            db.DSMH = bus.get_ALL_MH();
            //}
        }

        private void btnThem_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            Them_MonHoc MH = new Them_MonHoc();
            MH.ShowDialog();

            var db = this.FindResource("DSMH") as DanhSachMonHoc;
            db.DSMH = bus.get_ALL_MH();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            LoadMonHoc();
        }
        MonHocBUS bus = new MonHocBUS();
        private void btnXoa_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            int mamh;
            mamh = Convert.ToInt32(grdMonHoc.GetFocusedRowCellValue("MaMH").ToString());
            if(bus.kiemtra_xoa(mamh) == "true")
            {
                bus.deleteMH(mamh);

                var db = this.TryFindResource("DSMH") as DanhSachMonHoc;
                db.DSMH = bus.get_ALL_MH();
            }
            else
            {
                MessageBox.Show(bus.kiemtra_xoa(mamh));
            }
            

        }

        private void btnSua_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            Them_MonHoc MH = new Them_MonHoc();
            MH.monhoc.MaMH = Convert.ToInt32(grdMonHoc.GetFocusedRowCellValue("MaMH").ToString());
            MH.monhoc.TenMH = grdMonHoc.GetFocusedRowCellValue("TenMH").ToString();
            MH.monhoc.DiemChuan = Convert.ToInt32(grdMonHoc.GetFocusedRowCellValue("DiemChuan").ToString());
            MH.monhoc.MaHK = Convert.ToInt32(grdMonHoc.GetFocusedRowCellValue("MaHK").ToString());
            MH.kiemtra = 1;
            MH.ShowDialog();

            var db = this.FindResource("DSMH") as DanhSachMonHoc;
            db.DSMH = bus.get_ALL_MH();

        }
    }
}
