﻿using _041_112_124_138.special_class;
using BUS;
using DAO;
using DevExpress.Xpf.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _041_112_124_138
{
    /// <summary>
    /// Interaction logic for UC_BangDiem.xaml
    /// </summary>
    public partial class UC_BangDiem : UserControl
    {
        public UC_BangDiem()
        {
            InitializeComponent();
            LoadBangDiem();
        }
        DiemBUS dbus = new DiemBUS();
        public void LoadBangDiem()
        {

            
            //List<Diem> lst = dbus.getAll_Diem();
            //var db = this.FindResource("dsbd") as DanhSachBangDiem;
            //db.DSBD = dbus.getAll_Diem();

            LopBUS lopbus = new LopBUS();
            cbLop.DataContext = lopbus.getAll_Lop();
            cbLop.SelectedValuePath = "MaLop";
            cbLop.DisplayMemberPath = "TenLop";
            cbLop.SelectedIndex = 0;

            //grdHocSinh.ItemsSource = lst.ToList();
            HocKyBUS hkbus = new HocKyBUS();
          
            List<HocKy> listHK = hkbus.get_ALL_HK();
            var db1 = this.FindResource("dshk") as DanhSachHocKi;
            db1.DSHK = listHK;
            
            cbHK.SelectedValuePath = "MonHocs";
            cbHK.DisplayMemberPath = "TenHK";
            cbHK.SelectedIndex = 0;
            MonHocBUS mhbus = new MonHocBUS();

            //ObservableCollection<MonHoc> listMH = new ObservableCollection<MonHoc>(mhbus.get_ALL_MH());
            //cbMH.DataContext = listMH;
            cbMH.SelectedValuePath = "MaMH";
            cbMH.DisplayMemberPath = "TenMH";
            cbMH.SelectedIndex = 0;


        }

        private void btnSua_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            ThemDiem th = new ThemDiem();
            th.kiemtra = 1;
            th.maHS = Convert.ToInt32(grdDiem.GetFocusedRowCellValue("MaHS").ToString());
            th.diem15 = Convert.ToDouble(grdDiem.GetFocusedRowCellValue("Diẹm15").ToString());
            th.diem1tiet = Convert.ToDouble(grdDiem.GetFocusedRowCellValue("Diem1tiet").ToString());
            th.diemcuoiky = Convert.ToDouble(grdDiem.GetFocusedRowCellValue("DiemCuoiHk").ToString());
            th.mahk = Convert.ToInt32(grdDiem.GetFocusedRowCellValue("MonHoc.MaHK").ToString());
            th.mamh = Convert.ToInt32(grdDiem.GetFocusedRowCellValue("MaMH").ToString());


            th.ShowDialog();

            cbHK.SelectedIndex = th.mahk;
            cbMH.SelectedValue = th.mamh;
            cbLop.SelectedValue = th.malop;

            List<Diem> lst = dbus.getMaLop_Diem(th.malop
              , th.mahk + 1, th.mamh);
            var db = this.FindResource("dsbd") as DanhSachBangDiem;
            db.DSBD = lst;

        }

        private void btnXoa_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            Diem d = new Diem();
            d.MaHS = Convert.ToInt32(grdDiem.GetFocusedRowCellValue("MaHS").ToString());
            d.MaMH = Convert.ToInt32(grdDiem.GetFocusedRowCellValue("MaMH").ToString());

            DiemBUS bus = new DiemBUS();
            bus.DeleteDiem(d);
            MessageBox.Show("Xóa thành công");
            Update_BangDiem();

        }

        private void btnThem_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            ThemDiem th = new ThemDiem();
            //th.maHS = Convert.ToInt32(grdDiem.GetFocusedRowCellValue("MaHS").ToString());
            th.kiemtra = 0;
            th.ShowDialog();

            //List<Diem> lst = dbus.getAll_Diem();
            //var db = this.FindResource("dsbd") as DanhSachBangDiem;
            //db.DSBD = lst;
            if(th.mahk != -1 && th.mamh != -1 && th.malop != -1)
            {
                cbHK.SelectedIndex = th.mahk;
                cbMH.SelectedValue = th.mamh;
                cbLop.SelectedValue = th.malop;

                List<Diem> lst = dbus.getMaLop_Diem(th.malop
                  , th.mahk + 1, th.mamh);
                var db = this.FindResource("dsbd") as DanhSachBangDiem;
                db.DSBD = lst;
            }
            
        }

        

        private void Update_BangDiem()
        {
            DiemBUS dbus = new DiemBUS();
            HashSet<MonHoc> mh = (HashSet<MonHoc>)cbHK.SelectedValue;

            if (cbLop.SelectedValue != null && mh != null && cbMH.SelectedValue != null)
            {
                int malop = Convert.ToInt32(cbLop.SelectedValue);
                int mahk = Convert.ToInt32(mh.FirstOrDefault().MaHK);
                int mamh = Convert.ToInt32(cbMH.SelectedValue);

                List<Diem> lst = dbus.getMaLop_Diem(malop
               , mahk, mamh);
                var db = this.FindResource("dsbd") as DanhSachBangDiem;
                db.DSBD = lst;
                if (lst.Count != 0)
                {
                    btnSua.IsVisible = true;
                    btnXoa.IsVisible = true;
                }
                else
                {
                    btnSua.IsVisible = false;
                    btnXoa.IsVisible = false;
                }
            }
        }
        private void grdHocSinh_SelectedItemChanged(object sender, DevExpress.Xpf.Grid.SelectedItemChangedEventArgs e)
        {
            btnSua.IsVisible = true;
            btnXoa.IsVisible = true;
        }

        private void btnXem_Click(object sender, RoutedEventArgs e)
        {
            if(cbMH.SelectedValue !=null)
            {
                Update_BangDiem();
            }
            else
            {
                MessageBox.Show("Bạn chưa chọn môn học");
            }
        }

        
    }
}
