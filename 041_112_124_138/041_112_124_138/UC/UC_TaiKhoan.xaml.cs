﻿using _041_112_124_138.special_class;
using BUS;
using DAO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _041_112_124_138
{
    /// <summary>
    /// Interaction logic for UC_TaiKhoan.xaml
    /// </summary>
    public partial class UC_TaiKhoan : UserControl
    {
        GiaoVienBUS bus = new GiaoVienBUS();
        public UC_TaiKhoan()
        {
            InitializeComponent();
        }

        private void btnThem_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            WD_Them_TaiKhoan themtaikhoa = new WD_Them_TaiKhoan();
            themtaikhoa.kiemtra_T_S = 0;
            themtaikhoa.ShowDialog();
            var db = this.FindResource("dsgv") as DanhSachGV;
            var ocMh = new ObservableCollection<GiaoVien>(bus.get_All_GiaoVien());
            db.DSGV = ocMh;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            
            var db = this.FindResource("dsgv") as DanhSachGV;
            var ocMh = new ObservableCollection<GiaoVien>(bus.get_All_GiaoVien());
            //grdHocSinh.ItemsSource = ocMh;
            //List<GiaoVien> lst = bus.get_All_GiaoVien();
            db.DSGV = ocMh;
        }

        private void btnXoa_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            int magv = Convert.ToInt32(grdHocSinh.GetFocusedRowCellValue("MaGV").ToString());
            bus.XoaGiaoVien(magv);
            var db = this.FindResource("dsgv") as DanhSachGV;
            var ocMh = new ObservableCollection<GiaoVien>(bus.get_All_GiaoVien());
            //grdHocSinh.ItemsSource = ocMh;
            //List<GiaoVien> lst = bus.get_All_GiaoVien();
            db.DSGV = ocMh;
            MessageBox.Show("Xóa Thành Công");
        }

        private void btnSua_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            GiaoVien gv = new GiaoVien();
            gv.MaGV = Convert.ToInt32(grdHocSinh.GetFocusedRowCellValue("MaGV").ToString());
            gv.UserName = grdHocSinh.GetFocusedRowCellValue("UserName").ToString();
            gv.TenGV = grdHocSinh.GetFocusedRowCellValue("TenGV").ToString();
            gv.PhanQuyen = Convert.ToInt32(grdHocSinh.GetFocusedRowCellValue("PhanQuyen").ToString());
            gv.PassWord = grdHocSinh.GetFocusedRowCellValue("PassWord").ToString();
            WD_Them_TaiKhoan themtaikhoa = new WD_Them_TaiKhoan();
            themtaikhoa.gv = gv;
            themtaikhoa.kiemtra_T_S = 1;
            themtaikhoa.ShowDialog();
            var db = this.FindResource("dsgv") as DanhSachGV;
            var ocMh = new ObservableCollection<GiaoVien>(bus.get_All_GiaoVien());
            db.DSGV = ocMh;
        }

        
    }
}
