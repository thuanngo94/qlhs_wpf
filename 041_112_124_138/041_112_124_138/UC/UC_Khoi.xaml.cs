﻿using _041_112_124_138.special_class;
using BUS;
using DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _041_112_124_138
{
    /// <summary>
    /// Interaction logic for UC_Khoi.xaml
    /// </summary>
    public partial class UC_Khoi : UserControl
    {
        public UC_Khoi()
        {
            InitializeComponent();
            Load();
        }
        public void Load()
        {
            KhoiBUS bus = new KhoiBUS();
            var db = this.FindResource("dskhoi") as DSKhoi;
            List<Khoi> list = bus.getAll_Khoi();
            db.DSKHOI = list;
        }
        private void btnThem_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            ThemKhoi khoi = new ThemKhoi();
            khoi.kt = 0;
            khoi.ShowDialog();
            Load();
        }

        private void btnSua_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            ThemKhoi khoi = new ThemKhoi();
            khoi.kt = 1;
            khoi.k2.MaKhoi = Convert.ToInt32(grdKhoi.GetFocusedRowCellValue("MaKhoi").ToString());
            khoi.k2.TenKhoi = grdKhoi.GetFocusedRowCellValue("TenKhoi").ToString();
            khoi.k2.TuoiToiThieu = Convert.ToInt32(grdKhoi.GetFocusedRowCellValue("TuoiToiThieu").ToString());
            khoi.k2.TuoiToiDa = Convert.ToInt32(grdKhoi.GetFocusedRowCellValue("TuoiToiDa").ToString());
            khoi.k2.SL_LopToiDa = Convert.ToInt32(grdKhoi.GetFocusedRowCellValue("SL_LopToiDa").ToString());
            khoi.ShowDialog();
            Load();

        }

        private void btnXoa_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            KhoiBUS bus = new KhoiBUS();
            int a = bus.xoakhoi(grdKhoi.GetFocusedRowCellValue("MaKhoi").ToString());
            if (a == 0)
            {
                MessageBox.Show("Khối có lớp ko xóa dc");
            }
            else
            {
                MessageBox.Show(" Xóa Khối Thành Công");
                Load();
            }
        }
    }
}
