﻿using _041_112_124_138.special_class;
using BUS;
using DevExpress.Xpf.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _041_112_124_138
{
    /// <summary>
    /// Interaction logic for UC_TimKiem_HocSinh.xaml
    /// </summary>
    public partial class UC_TimKiem_HocSinh : UserControl
    {
        public UC_TimKiem_HocSinh()
        {
            InitializeComponent();
            //DXSplashScreen.Show<SplashScreenWindow1>();
        }

        public void Load_All_HS()
        {
            HocSinhBUS bus = new HocSinhBUS();
            IEnumerable<Object> lst = bus.getall_hs_DTB("", "", 1);
            var db = this.FindResource("dshs") as DanhSachHocSinh;
            db.DSHS = lst;
           

        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Load_All_HS();
            //.DXSplashScreen.Close();
            LopBUS bus = new LopBUS();
            cbLop.ItemsSource = bus.getAll_Lop();
            cbLop.SelectedValuePath = "MaLop";
            cbLop.DisplayMemberPath = "TenLop";

        }

        private void btnTimKiem_Click(object sender, RoutedEventArgs e)
        {
            HocSinhBUS bus = new HocSinhBUS();
            if (cbLop.SelectedValue != null)
            {

                var db = this.FindResource("dshs") as DanhSachHocSinh;
                db.DSHS = null;
                db.DSHS = bus.getall_hs_DTB(txtTenHocSinh.Text,cbLop.SelectedValue.ToString(),2);

            }
            else
            {


                var db = this.FindResource("dshs") as DanhSachHocSinh;
                db.DSHS = null;
                db.DSHS = bus.getall_hs_DTB(txtTenHocSinh.Text,"", 3);
            }
        }
    }
}
