﻿using DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _041_112_124_138.special_class
{
    public sealed class DanhSachBangDiem: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        List<Diem> dsbd;
        public List<Diem> DSBD
        {
            get { return dsbd; }
            set
            {
                if (value == dsbd)
                {
                    return;
                }
                dsbd = value;
                OnPropertyChanged("DSBD");
            }
        }
    }
}
