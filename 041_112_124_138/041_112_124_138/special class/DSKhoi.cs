﻿using DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _041_112_124_138.special_class
{
    public sealed class DSKhoi : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        List<Khoi> ds;
        public List<Khoi> DSKHOI
        {
            get { return ds; }
            set
            {
                if (value == ds)
                {
                    return;
                }
                ds = value;
                OnPropertyChanged("DSKHOI");
            }
        }
    }
}
