﻿using DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _041_112_124_138.special_class
{
    public sealed class DanhSachMonHoc : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        List<MonHoc> ds;
        public List<MonHoc> DSMH
        {
            get { return ds; }
            set
            {
                if (value == ds)
                {
                    return;
                }
                ds = value;
                OnPropertyChanged("DSMH");
            }
        }

    }
    
}
