﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BUS;
using System.Windows.Forms;
using System.Data;
using DAO;
using _041_112_124_138.WDBaoCao;
namespace _041_112_124_138
{
    /// <summary>
    /// Interaction logic for HienBaoCao_TKMon.xaml
    /// </summary>
    public partial class HienBaoCao_TKMon : Window
    {
        public HienBaoCao_TKMon()
        {
            InitializeComponent();
        }
        public int MaMH = 0;
        public int MaHK = 0;
        public int kiemtra = 0;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ConnectionInfo connectionInfo = new ConnectionInfo();
            connectionInfo.ServerName = "ADMIN-PC\\WIN7";
            connectionInfo.UserID = "win7";
            connectionInfo.Password = "123456";
            connectionInfo.IntegratedSecurity = false;
            connectionInfo.DatabaseName = "QLHS";
            if(kiemtra == 0)
            {
                BaoCaoTK_Mon bc = new BaoCaoTK_Mon();

                


                bc.SetParameterValue("@maMon", MaMH);
                bc.SetParameterValue("@maHk", MaHK);


                CrystalDecisions.CrystalReports.Engine.Tables tables = bc.Database.Tables;

                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    CrystalDecisions.Shared.TableLogOnInfo tableLogOnInfo = table.LogOnInfo;
                    tableLogOnInfo.ConnectionInfo = connectionInfo;
                    table.ApplyLogOnInfo(tableLogOnInfo);
                }

                //Load the report by setting the report source
                CrystalReportViewer1.ViewerCore.ReportSource = bc;
            }
            else
            {
                XemBaoCaoHocKy bc = new XemBaoCaoHocKy();
                bc.SetParameterValue("@maHk", MaHK);


                CrystalDecisions.CrystalReports.Engine.Tables tables = bc.Database.Tables;

                foreach (CrystalDecisions.CrystalReports.Engine.Table table in tables)
                {
                    CrystalDecisions.Shared.TableLogOnInfo tableLogOnInfo = table.LogOnInfo;
                    tableLogOnInfo.ConnectionInfo = connectionInfo;
                    table.ApplyLogOnInfo(tableLogOnInfo);
                }

                //Load the report by setting the report source
                CrystalReportViewer1.ViewerCore.ReportSource = bc;
            }


            
        }

       
    }
}
